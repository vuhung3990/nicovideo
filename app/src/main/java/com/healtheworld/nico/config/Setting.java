package com.healtheworld.nico.config;

/**
 * Created by YuHung on 4/28/2015.
 */
public class Setting {
    /**
     * add this to base64encode for more difficult to crack
     *
     * @return
     */
    public static String getDefaultKey() {
        return "sdfghiuo";
    }

    /**
     * @return table account name
     */
    public static String getTableAccountName() {
        return "account";
    }

    /**
     * @return database version
     */
    public static int getDatabaseVerson() {
        return 8;
    }

    /**
     * @return database name
     */
    public static String getDatabaseName() {
        return "database.db";
    }

    /**
     * @return log tag
     */
    public static String getLogTag() {
        return "@LOG_ME@@";
    }

    /**
     * @return table name: fav container
     */
    public static String getTableFavContainer() {
        return "fav_container";
    }

    /**
     * @return table name: fav item
     */
    public static String getTableFavItem() {
        return "fav_item";
    }

    /**
     * @return count down for show ads
     */
    public static int getAdsCountDown() {
        return 30;
    }
}
