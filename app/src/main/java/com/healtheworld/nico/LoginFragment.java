package com.healtheworld.nico;

import android.app.Fragment;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.healtheworld.nico.helper.database.DatabaseUtils;
import com.healtheworld.nico.helper.common.NicoUtils;

import org.apache.http.Header;

/**
 * Created by VuHung on 4/18/2015.
 */
public class LoginFragment extends Fragment {
    private Button login_button;
    private RelativeLayout login_container;
    private EditText login_email;
    private EditText login_password;
    private LoginEventCallback loginEventCallback;
    private FrameLayout btn_login_container;

    /**
     * set view group and children is enable/disable
     *
     * @param view    view group
     * @param enabled true: enable, false: disable
     */
    private static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    /**
     * set callback
     */
    public void setLoginEventCallback(LoginEventCallback loginEventCallback) {
        this.loginEventCallback = loginEventCallback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        login_button = (Button) view.findViewById(R.id.login_button);
        login_email = (EditText) view.findViewById(R.id.login_email);
        login_password = (EditText) view.findViewById(R.id.login_pasword);

        btn_login_container = (FrameLayout) view.findViewById(R.id.btn_login_container);
        login_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                login_password.setError(null);
                btn_login_container.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        // container
        login_container = (RelativeLayout) view.findViewById(R.id.login_container);

        stopAnimation(login_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check email valid, trim & password not null
                String email = login_email.getText().toString().trim();
                if (isValidEmailAddress(email)) {
                    if (login_password.getText().toString().trim().length() > 6) {
                        startAnimation(login_button);
                        //  check fragment_login from server
                        NicoUtils.getAccountDetails(getActivity(), login_email.getText().toString().trim(), login_password.getText().toString().trim(), new NicoUtils.getAccountDetailsCallback() {
                            @Override
                            public void onLoginFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                resetViews();
                                Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.login_fail, Snackbar.LENGTH_LONG).show();
                            }

                            @Override
                            public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.login_success_but_error_when_get_info, Snackbar.LENGTH_LONG).show();
                            }

                            @Override
                            public void onSuccess(String email, String userName, String avatarUrl) {
                                Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.login_success, Snackbar.LENGTH_LONG).show();
                                // stop animation
                                stopAnimation(login_button);

                                // save to database
                                DatabaseUtils.cleanAndAddAccount(email, login_password.getText().toString().trim(), userName, avatarUrl);
                                if (loginEventCallback != null) {
                                    loginEventCallback.onLoginSuccess(email, userName, avatarUrl);
                                }
                            }
                        });
                    } else {
                        login_password.setError(getString(R.string.pass_leng_invalid));
                        btn_login_container.setVisibility(View.GONE);
                        login_password.requestFocus();
                    }
                } else {
                    login_email.setError(getString(R.string.email_not_valid));
                    login_email.requestFocus();
                }
            }
        });

        // set text link clickable
        TextView login_forgot_password = (TextView) view.findViewById(R.id.login_forgot_password);
        login_forgot_password.setText(Html.fromHtml("<a href='https://secure.nicovideo.jp/secure/remind_pass'>" + getString(R.string.forgot_password) + "</a>"));
        login_forgot_password.setMovementMethod(LinkMovementMethod.getInstance());

        // set text link clickable
        TextView login_register = (TextView) view.findViewById(R.id.login_register);
        login_register.setText(Html.fromHtml("<a href='https://account.nicovideo.jp/register/email?site=spniconico'>" + getString(R.string.create_account) + "</a>"));
        login_register.setMovementMethod(LinkMovementMethod.getInstance());

        return view;
    }

    /**
     * call when login fail, restore views
     */
    private void resetViews() {
        // re-active all views
        setViewAndChildrenEnabled(login_container, true);

        // clear incorrect data
//        login_email.setText("");
        login_password.setText("");

        login_password.requestFocus();
        stopAnimation(login_button);
    }

    /**
     * start loading animation
     *
     * @param button destination
     */
    public void startAnimation(Button button) {
        // set drawable
        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.rotating_loading, 0, 0, 0);
        Drawable[] drawables = button.getCompoundDrawables();
        for (Drawable drawable : drawables) {
            if (drawable != null && drawable instanceof Animatable) {
                ((Animatable) drawable).start();
            }
        }

        setViewAndChildrenEnabled(login_container, false);
    }

    /**
     * stop loading animation
     *
     * @param button destination
     */
    private void stopAnimation(Button button) {
        // set drawable
        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.login_button_go, 0, 0, 0);
        button.setAnimation(null);
        setViewAndChildrenEnabled(login_container, true);
    }

    /**
     * check email format valid ?
     *
     * @param email string email to check
     * @return true if valid
     */
    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public interface LoginEventCallback {
        /**
         * when login success
         *
         * @param email     current email
         * @param userName  current user's name
         * @param avatarUrl avatar url
         */
        void onLoginSuccess(String email, String userName, String avatarUrl);
    }
}
