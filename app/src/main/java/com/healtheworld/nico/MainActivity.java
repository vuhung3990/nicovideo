package com.healtheworld.nico;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.healtheworld.nico.custom.navigation.NavigationDrawerCallbacks;
import com.healtheworld.nico.custom.navigation.NavigationDrawerFragment;
import com.healtheworld.nico.custom.navigation.NavigationItem;
import com.healtheworld.nico.custom.recycleview.MyScrollListener;
import com.healtheworld.nico.custom.recycleview.RecycleAdapter;
import com.healtheworld.nico.custom.recycleview.RecycleItemObject;
import com.healtheworld.nico.helper.common.NicoUtils;
import com.healtheworld.nico.helper.common.Utils;
import com.healtheworld.nico.helper.database.AccountObject;
import com.healtheworld.nico.helper.database.DatabaseUtils;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements NavigationDrawerCallbacks, SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, LoginFragment.LoginEventCallback, RecycleAdapter.ItemSubMenu {
    /**
     * tag for find fragment fragment_login
     */
    private static final String TAG_FRAGMENT_LOGIN = "fragment_login";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * time to detect exit
     */
//    private long back_pressed = 0;

    /**
     * recycle list contain main data
     */
    private RecyclerView recycleView;
    private RecycleAdapter adapter;

    /**
     * swipe layout contain main recycle list
     */
    private SwipeRefreshLayout mSwipeRefreshLayout;

    /**
     * list object of main recycle view
     */
    private List<RecycleItemObject> objects;
    private MyScrollListener mScrollEvent;
    private Toolbar mToolbar;
    private Activity mActivity;

    /**
     * save current list object for restore when back from search
     */
    private List<RecycleItemObject> mSaveCurrentObjectsForSearchViewBack;

    /**
     * true when expand search view
     */
    private boolean isExpandingSearch = false;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private int currentSearchPage = 0;
    private String currentSearchText;

    /**
     * current category ({@link NicoUtils#CATEGORY_ALL}, {@link NicoUtils#CATEGORY_ANIME_ART_GAMES}...)or name (db) to load, default is load all
     */
    private String currentCategory = NicoUtils.CATEGORY_ALL;
    private MyApplication mGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // global var
        mGlobal = (MyApplication) getApplicationContext();

        // toolbar setup
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);

        // fix warning null when getSupportActionBar().isShowing()
        assert getSupportActionBar() != null;

        // save current activity
        mActivity = this;


        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        mActionBarDrawerToggle = mNavigationDrawerFragment.getActionBarDrawerToggle();
        mActionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // event when click back button when search expanded
                backFromSearch();
            }
        });

        // setup dialog logout
        AlertDialog.Builder builderLogout = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        builderLogout.setMessage(R.string.confirm_logout_message);
        builderLogout.setPositiveButton(R.string.positive_label_btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // clear account in db and reset profile views
                mNavigationDrawerFragment.resetAccountInfo();
            }
        });
        builderLogout.setNegativeButton(R.string.negative_label_btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog logoutDialog = builderLogout.create();

        mNavigationDrawerFragment.setProfileListener(new NavigationDrawerFragment.ProfileCallback() {
            @Override
            public void onProfileLogout() {
                logoutDialog.show();
            }

            @Override
            public void onProfileLogin() {
                showFragmentLogin();
            }
        });

        // check fragment_login account
        if (DatabaseUtils.getAccountSaved() == null) {
            showFragmentLogin();
        }

        // setup recycle list
        recycleView = (RecyclerView) findViewById(R.id.recycle_list);
        recycleView.setHasFixedSize(true);

        objects = new ArrayList<>();
        adapter = new RecycleAdapter(this, objects, this);

        // init layout orientation
        adapter.setLayoutOrientation(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE, recycleView);
        recycleView.setAdapter(adapter);

        // scroll down to hide toolbar...
        mScrollEvent = new MyScrollListener(adapter) {
            @Override
            protected void onLoadMore() {
                // loadmore
                if (currentSearchPage != -1) {
                    adapter.showLoadMore(true);
                    mSwipeRefreshLayout.setRefreshing(false);

                    // because load more only show in search function
                    search();
                }
            }

            @Override
            protected void onHide() {
//                if (getSupportActionBar().isShowing()) getSupportActionBar().hide();
            }

            @Override
            protected void onShow() {
//                if (!getSupportActionBar().isShowing()) getSupportActionBar().show();
            }
        };
        recycleView.addOnScrollListener(mScrollEvent);

        // setup pull to refresh and load more
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_main);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.color_scheme_swipe_layout1,
                R.color.color_scheme_swipe_layout2,
                R.color.color_scheme_swipe_layout3,
                R.color.color_scheme_swipe_layout4);
//        mSwipeRefreshLayout.setSize(SwipeRefreshLayout.LARGE);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        // refresh data when start, load default all
        onRefresh();
    }

    /**
     * play video in other activity for better performance
     *
     * @param videoID video id
     */
    private void playVideo(String videoID, String title) {
        // check login fist
        if (DatabaseUtils.getAccountSaved() == null) {
            showFragmentLogin();
        } else {
            Intent intent = new Intent(this, ViewVideoActivity.class);
            intent.putExtra(ViewVideoActivity.KEY_VIDEO_ID, videoID);
            intent.putExtra(ViewVideoActivity.KEY_VIDEO_TITLE, title);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
        }
    }

    /**
     * hide fragment fragment_login
     */
    private void hideFragmentLogin() {
        getFragmentManager()
                .beginTransaction()
                        // custom animation
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .remove(getFragmentManager().findFragmentByTag(TAG_FRAGMENT_LOGIN)).commit();
    }

    /**
     * check fragment_login fragment is visible ?
     *
     * @return true: showing, else hide
     */
    private boolean isLoginVisible() {
        Fragment loginFragment = getFragmentManager().findFragmentByTag(TAG_FRAGMENT_LOGIN);
        return loginFragment != null;
    }

    /**
     * show fragment fragment_login
     */
    private void showFragmentLogin() {
        // fix issue #2 don't allow fragment fragment_login multiple
        if (!isLoginVisible()) {
            LoginFragment loginFragment = new LoginFragment();
            // set callback
            loginFragment.setLoginEventCallback(this);
            // commit fragment & custom animation
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .add(R.id.main_container, loginFragment, TAG_FRAGMENT_LOGIN).commitAllowingStateLoss();

            // hide drawer
            mNavigationDrawerFragment.closeDrawer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_activity, menu);

            MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
            if (mSearchMenuItem != null) {
                final SearchView mSearchView = (SearchView) mSearchMenuItem.getActionView();
                mSearchView.setOnQueryTextListener(this);
                mSearchView.setOnSearchClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // expand search view
                        isExpandingSearch = true;

                        // disable home button & change icon
                        mActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);

                        // #1 save to restore data.(see more #2)
                        mSaveCurrentObjectsForSearchViewBack = new ArrayList<>();
                        mSaveCurrentObjectsForSearchViewBack.addAll(objects);

                        // clear data & notify adapter
                        objects.clear();
                        adapter.notifyDataSetChanged();

                        // disable refresh func when expand search
                        mSwipeRefreshLayout.setEnabled(false);
                        mSwipeRefreshLayout.setRefreshing(false);

                        // enable load more function
                        mScrollEvent.setIsActiveLoadmore(true);
                    }
                });
                mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        //  close search view
                        // #2 back previous state & restore data previous
                        backFromSearch();
                        return true;
                    }
                });
            }

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * back from search to normal main activity
     */
    private void backFromSearch() {
        // dimiss progress & search thread
        mSwipeRefreshLayout.setRefreshing(false);

        // hide soft keyboard
        Utils.hideSoftKeyboard(mActivity);
        // close search view
        invalidateOptionsMenu();
        // restore drawer icon
        mActionBarDrawerToggle.syncState();

        // restore data
        if (mSaveCurrentObjectsForSearchViewBack != null) {
            objects.clear();
            // add to list object and free mSaveCurrentObjectsForSearchViewBack then notify adapter
            objects.addAll(mSaveCurrentObjectsForSearchViewBack);
            mSaveCurrentObjectsForSearchViewBack = null;

            //=========== fix #30 =================//
            // re-config item layout
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                adapter.setLayoutOrientation(false, recycleView);
            } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                adapter.setLayoutOrientation(true, recycleView);
            }
            //====================================//
        }

        // set state search expand
        isExpandingSearch = false;

        // re-enable home button to show drawer
        mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);

        // re-enable refresh func when collapse search
        mSwipeRefreshLayout.setEnabled(true);

        // disable load more function
        mScrollEvent.setIsActiveLoadmore(false);

        // reset current search page to zero
        currentSearchPage = 0;

        // reset current search text
        currentSearchText = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isExpandingSearch) {
            // if search view is expanded -> close
            backFromSearch();
        } else if (mNavigationDrawerFragment.isDrawerOpen())
            // if navigation opened -> close
            mNavigationDrawerFragment.closeDrawer();
        else {
            // check if fragment_login fragment is showing ? true -> dimiss
            if (isLoginVisible()) {
                hideFragmentLogin();
            }
            // double back to exit
            else {
                mGlobal.showInterstitialAds(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        finish();
                    }
                });
//                if (back_pressed + 2000 > System.currentTimeMillis()) {
//                    super.onBackPressed();
//                } else {
//                    Snackbar.make(findViewById(android.R.id.content), R.string.double_press_to_exit, Snackbar.LENGTH_SHORT).show();
//                }
//                back_pressed = System.currentTimeMillis();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // re-generate navigation for case back from video view activity
        onAddPlayList();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // change layout when orientation
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            adapter.setLayoutOrientation(false, recycleView);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            adapter.setLayoutOrientation(true, recycleView);
        }
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        objects.clear();
        adapter.notifyDataSetChanged();

        // change title
        String title = null;
        switch (currentCategory) {
            case NicoUtils.CATEGORY_ALL:
                title = getString(R.string.category_all);
                break;
            case NicoUtils.CATEGORY_ENTERTAIMENT_MUSIC:
                title = getString(R.string.category_entertaiment_music);
                break;
            case NicoUtils.CATEGORY_ENTERTAIMENT:
                title = getString(R.string.category_entertaiment);
                break;
            case NicoUtils.CATEGORY_MUSIC:
                title = getString(R.string.category_music);
                break;
            case NicoUtils.CATEGORY_SANG_IT:
                title = getString(R.string.category_sang_it);
                break;
            case NicoUtils.CATEGORY_PLAYED_IT:
                title = getString(R.string.category_played_it);
                break;
            case NicoUtils.CATEGORY_DANCED_IT:
                title = getString(R.string.category_danced_it);
                break;
            case NicoUtils.CATEGORY_VOCALOID:
                title = getString(R.string.category_vocaloid);
                break;
            case NicoUtils.CATEGORY_NICO_INDIES:
                title = getString(R.string.category_nico_indies);
                break;
            case NicoUtils.CATEGORY_LIFE_GENERAL_SPORT:
                title = getString(R.string.category_life_general_sport);
                break;
            case NicoUtils.CATEGORY_ANIMALS:
                title = getString(R.string.category_animals);
                break;
            case NicoUtils.CATEGORY_COOKING:
                title = getString(R.string.category_cooking);
                break;
            case NicoUtils.CATEGORY_NATURE:
                title = getString(R.string.category_nature);
                break;
            case NicoUtils.CATEGORY_TRAVELING:
                title = getString(R.string.category_traveling);
                break;
            case NicoUtils.CATEGORY_SPORTS:
                title = getString(R.string.category_sports);
                break;
            case NicoUtils.CATEGORY_NICO_LECTURE:
                title = getString(R.string.category_nico_lecture);
                break;
            case NicoUtils.CATEGORY_VEHICLES:
                title = getString(R.string.category_vehicles);
                break;
            case NicoUtils.CATEGORY_HISTORY:
                title = getString(R.string.category_history);
                break;
            case NicoUtils.CATEGORY_POLITICS:
                title = getString(R.string.category_politics);
                break;
            case NicoUtils.CATEGORY_SCIENCE_TECH:
                title = getString(R.string.category_science_tech);
                break;
            case NicoUtils.CATEGORY_SCIENCE:
                title = getString(R.string.category_science);
                break;
            case NicoUtils.CATEGORY_NICO_TECH:
                title = getString(R.string.category_nico_tech);
                break;
            case NicoUtils.CATEGORY_NICO_CRAFTS:
                title = getString(R.string.category_craft);
                break;
            case NicoUtils.CATEGORY_MADE_IT:
                title = getString(R.string.category_made_it);
                break;
            case NicoUtils.CATEGORY_ANIME_ART_GAMES:
                title = getString(R.string.category_anime_art_game);
                break;
            case NicoUtils.CATEGORY_ANIME:
                title = getString(R.string.category_anime);
                break;
            case NicoUtils.CATEGORY_GAMES:
                title = getString(R.string.category_games);
                break;
            case NicoUtils.CATEGORY_TOUHOU:
                title = getString(R.string.category_touhou);
                break;
            case NicoUtils.CATEGORY_IDOLMASTER:
                title = getString(R.string.category_idolmaster);
                break;
            case NicoUtils.CATEGORY_RADIO:
                title = getString(R.string.category_radio);
                break;
            case NicoUtils.CATEGORY_DREW_IT:
                title = getString(R.string.category_drew_it);
                break;
            case NicoUtils.CATEGORY_OTHERS_ALL:
                title = getString(R.string.category_other);
                break;
            case NicoUtils.CATEGORY_THAT_THING:
                title = getString(R.string.category_that_thing);
                break;
            case NicoUtils.CATEGORY_DIARY:
                title = getString(R.string.category_diary);
                break;
            case NicoUtils.CATEGORY_OTHERS:
                title = getString(R.string.category_others);
                break;
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);

        // GET CURRENT CATEGORY, and device language
        String currentLanguage = Locale.getDefault().getLanguage().equalsIgnoreCase("ja") ? NicoUtils.LANG_JAP : NicoUtils.LANG_ENG;
        NicoUtils.getDailyRanking(currentCategory, currentLanguage, new NicoUtils.getDailyRankingCallback() {
            @Override
            public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                adapter.disableLoadmoreAndNotifyDataChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onSuccess(List<RecycleItemObject> list) {
                if (!isExpandingSearch) {
                    objects.addAll(list);
                    adapter.disableLoadmoreAndNotifyDataChanged();
                    mSwipeRefreshLayout.setRefreshing(false);

                    // disable load more function
                    mScrollEvent.setIsActiveLoadmore(false);

                    //=========== fix #30 =================//
                    // re-config item layout
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                        adapter.setLayoutOrientation(false, recycleView);
                    } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        adapter.setLayoutOrientation(true, recycleView);
                    }
                    //===================================//

                    // scroll to fist
                    recycleView.scrollToPosition(0);
                } else {
                    mSaveCurrentObjectsForSearchViewBack = new ArrayList<>();
                    mSaveCurrentObjectsForSearchViewBack = list;
                }
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        currentSearchText = query.trim();

        if (DatabaseUtils.getAccountSaved() != null) {
            // =====================#35=========================//
            // show loading progress
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });

            // clear data
            objects.clear();
            adapter.notifyDataSetChanged();
            // =================================================//

            search();
        } else {
            // clear focus
            getWindow().getDecorView().clearFocus();
            // if not login show login fragment
            showFragmentLogin();
        }
        return false;
    }

    /**
     * search with fixed data
     *
     * @see #currentSearchPage
     * @see #currentSearchText
     */
    private void search() {
        AccountObject currentAccount = DatabaseUtils.getAccountSaved();
        if (currentAccount != null) {
            // clear focus
            getWindow().getDecorView().clearFocus();

            // search submit
            NicoUtils.searchVideo(this, currentSearchPage + 1, currentSearchText, currentAccount.getEmail(), currentAccount.getPassword(), new NicoUtils.searchVideoCallback() {
                @Override
                public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    adapter.disableLoadmoreAndNotifyDataChanged();
                }

                @Override
                public void onLoginFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    adapter.disableLoadmoreAndNotifyDataChanged();
                }

                @Override
                public void onSuccess(int page, List<RecycleItemObject> list) {
                    // hide progress
                    mSwipeRefreshLayout.setRefreshing(false);

                    // only update data when expanding search
                    if (isExpandingSearch) {
                        // save current search page
                        currentSearchPage = page;

                        if (page != -1) {
                            // add to list adapter and notify
                            adapter.showLoadMore(false);
                            list = adapter.addFavItemIntoListData(list);
                            objects.addAll(list);

                            // only notify inserted rage
                            adapter.notifyItemRangeInserted(objects.size() - list.size(), list.size());
                        } else {
                            adapter.showLoadMore(false);
                        }
                    }
                }
            });
        } else {
            // if not login show login fragment
            showFragmentLogin();
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // TODO: seach hint here
        return false;
    }

    @Override
    public void onLoginSuccess(String email, String userName, String avatarUrl) {
        // hide login fragment
        hideFragmentLogin();

        // set avatar, name, email
        mNavigationDrawerFragment.setAccountInfo(email, userName, avatarUrl);

        // open navigation drawer
        mNavigationDrawerFragment.openDrawer();

        // close search view
        backFromSearch();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position, String category, boolean isFromLocal, boolean reloadData) {
        if (recycleView != null) {
            if (isFromLocal) {
                // disable refresh
                mSwipeRefreshLayout.setRefreshing(false);
                mSwipeRefreshLayout.setEnabled(false);

                // load data from db
                objects.clear();
                objects.addAll(DatabaseUtils.getPlaylistByName(category));
                adapter.notifyDataSetChanged();

                // set title
                if (getSupportActionBar() != null)
                    getSupportActionBar().setTitle(category);
            } else {
                // enable refresh
                mSwipeRefreshLayout.setEnabled(true);

                // set current categoty
                currentCategory = category;

                // only refresh data if reload param true
                if (reloadData)
                    onRefresh();
            }
        }
    }

    @Override
    public void onShare(int itemPosition) {
        Utils.shareTextUrl(this, NicoUtils.BASE_VIDEO_LINK + objects.get(itemPosition).getId());
    }

    @Override
    public void onPlay(int itemPosition) {
        playVideo(objects.get(itemPosition).getId(), objects.get(itemPosition).getTitle());
    }

    @Override
    public void onAddPlayList() {
        // save current navigation object for set right position later #43
        NavigationItem currentItem = mNavigationDrawerFragment.getCurrentItem();

        // re-generate navigation items
        mNavigationDrawerFragment.generateNavigationItems();

        // select item pre-generate
        mNavigationDrawerFragment.setCurrentSelectedPosition(currentItem);
    }

    @Override
    public void onItemClick(final int itemPosition) {
        if (mGlobal.isEndAdsCountDown()) {
            mGlobal.showInterstitialAds(new AdListener() {
                @Override
                public void onAdClosed() {
                    onPlay(itemPosition);
                }
            });
        } else {
            onPlay(itemPosition);
        }
    }
}