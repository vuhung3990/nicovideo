package com.healtheworld.nico.custom.navigation;


import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.healtheworld.nico.R;
import com.healtheworld.nico.helper.common.NicoUtils;
import com.healtheworld.nico.helper.common.Utils;

import java.util.List;


public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    private List<NavigationItem> mData;
    private NavigationDrawerCallbacks mNavigationDrawerCallbacks;
    private View mSelectedView;
    private int mSelectedPosition;
    private Context context;

    public NavigationDrawerAdapter(List<NavigationItem> data) {
        mData = data;
    }

//    public NavigationDrawerCallbacks getNavigationDrawerCallbacks() {
//        return mNavigationDrawerCallbacks;
//    }

    public void setNavigationDrawerCallbacks(NavigationDrawerCallbacks navigationDrawerCallbacks) {
        mNavigationDrawerCallbacks = navigationDrawerCallbacks;
    }

    @Override
    public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {
        context = viewGroup.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.drawer_row, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(v);
        if (viewtype == NavigationItem.NAVIGATION_VIEW_TYPE_ITEM) {
            viewHolder.itemView.setClickable(true);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectedView != null) {
                        mSelectedView.setSelected(false);
                    }
                    mSelectedPosition = viewHolder.getAdapterPosition();
                    v.setSelected(true);
                    mSelectedView = v;

                    // only in selected normal item ( not in sub menu )
                    if (mNavigationDrawerCallbacks != null) {
                        // caculate item selected position
                        int normalItemSelected = mData.size() - mSelectedPosition;
                        // save normal selected category & calculate category
                        String normalItemSelectedCategory;
                        switch (normalItemSelected) {
                            case 1:
                                normalItemSelectedCategory = NicoUtils.CATEGORY_OTHERS_ALL;
                                break;
                            case 2:
                                normalItemSelectedCategory = NicoUtils.CATEGORY_ANIME_ART_GAMES;
                                break;
                            case 3:
                                normalItemSelectedCategory = NicoUtils.CATEGORY_SCIENCE_TECH;
                                break;
                            case 4:
                                normalItemSelectedCategory = NicoUtils.CATEGORY_POLITICS;
                                break;
                            case 5:
                                normalItemSelectedCategory = NicoUtils.CATEGORY_LIFE_GENERAL_SPORT;
                                break;
                            case 6:
                                normalItemSelectedCategory = NicoUtils.CATEGORY_ENTERTAIMENT_MUSIC;
                                break;
                            case 7:
                                normalItemSelectedCategory = NicoUtils.CATEGORY_ALL;
                                break;
                            default:
                                normalItemSelectedCategory = mData.get(mSelectedPosition).getText();
                                break;
                        }
                        mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(viewHolder.getAdapterPosition(), normalItemSelectedCategory, false, true);
                    }
                }
            });
            // set background state
            viewHolder.itemView.setBackgroundResource(R.drawable.row_selector);

            // more padding left
            viewHolder.name.setPadding(Utils.dpToPx(context, 25), 0, 0, 0);
            viewHolder.name.setTextAppearance(context, R.style.Base_TextAppearance_AppCompat_SearchResult);
        } else if (viewtype == NavigationItem.NAVIGATION_VIEW_TYPE_LABEL) {
            // adjust container smaller
            viewHolder.itemView.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
            // set gravity bottom
            viewHolder.name.setGravity(Gravity.BOTTOM);
            // set underline
            viewHolder.name.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
            // set text style
            viewHolder.name.setTextAppearance(context, R.style.Base_TextAppearance_AppCompat_SearchResult_Subtitle);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final NavigationDrawerAdapter.ViewHolder viewHolder, final int position) {
        final NavigationItem data = mData.get(position);

        // item name
        viewHolder.name.setText(data.getText());

        // item count
        viewHolder.count.setVisibility(data.getCount() == -1 ? View.GONE : View.VISIBLE);
        viewHolder.count.setText(String.format("%d", data.getCount()));

        // item menu
        viewHolder.menu.setVisibility(data.getShowMenuButton() ? View.VISIBLE : View.GONE);
        // set menu -> show popup
        viewHolder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.getResSubMenu() != -1) {
                    PopupMenu popupMenu = new PopupMenu(context, viewHolder.menu);
                    popupMenu.getMenuInflater().inflate(data.getResSubMenu(), popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.category_animals:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_ANIMALS, false, true);
                                    return true;
                                case R.id.category_anime:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_ANIME, false, true);
                                    return true;
                                case R.id.category_cooking:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_COOKING, false, true);
                                    return true;
                                case R.id.category_danced_it:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_DANCED_IT, false, true);
                                    return true;
                                case R.id.category_diary:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_DIARY, false, true);
                                    return true;
                                case R.id.category_drew_it:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_DREW_IT, false, true);
                                    return true;
                                case R.id.category_entertaiment:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_ENTERTAIMENT, false, true);
                                    return true;
                                case R.id.category_games:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_GAMES, false, true);
                                    return true;
                                case R.id.category_history:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_HISTORY, false, true);
                                    return true;
                                case R.id.category_idolmaster:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_IDOLMASTER, false, true);
                                    return true;
                                case R.id.category_made_it:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_MADE_IT, false, true);
                                    return true;
                                case R.id.category_music:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_MUSIC, false, true);
                                    return true;
                                case R.id.category_nature:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_NATURE, false, true);
                                    return true;
                                case R.id.category_nico_crafts:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_NICO_CRAFTS, false, true);
                                    return true;
                                case R.id.category_nico_indies:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_NICO_INDIES, false, true);
                                    return true;
                                case R.id.category_nico_lecture:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_NICO_LECTURE, false, true);
                                    return true;
                                case R.id.category_nico_tech:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_NICO_TECH, false, true);
                                    return true;
                                case R.id.category_others:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_OTHERS, false, true);
                                    return true;
                                case R.id.category_played_it:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_PLAYED_IT, false, true);
                                    return true;
                                case R.id.category_radio:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_RADIO, false, true);
                                    return true;
                                case R.id.category_sang_it:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_SANG_IT, false, true);
                                    return true;
                                case R.id.category_science:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_SCIENCE, false, true);
                                    return true;
                                case R.id.category_sports:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_SPORTS, false, true);
                                    return true;
                                case R.id.category_that_thing:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_THAT_THING, false, true);
                                    return true;
                                case R.id.category_touhou:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_TOUHOU, false, true);
                                    return true;
                                case R.id.category_traveling:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_TRAVELING, false, true);
                                    return true;
                                case R.id.category_vehicles:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_VEHICLES, false, true);
                                    return true;
                                case R.id.category_vocaloid:
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position, NicoUtils.CATEGORY_VOCALOID, false, true);
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popupMenu.show();
                }
            }
        });

        // set navigation color background
        if (mSelectedPosition == position) {
            viewHolder.itemView.setSelected(true);
        } else {
            viewHolder.itemView.setSelected(false);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mData != null ? mData.get(position).getType() : 0;
    }

    public void selectPosition(int position) {
        mSelectedPosition = position;

        // refresh navigation view
        for (int i = 0; i < mData.size(); i++) {
            notifyItemChanged(i);
        }
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView count;
        public ImageButton menu;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.item_name);
            count = (TextView) itemView.findViewById(R.id.item_count);
            menu = (ImageButton) itemView.findViewById(R.id.item_menu);
        }
    }
}