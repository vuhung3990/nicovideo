package com.healtheworld.nico.custom.dialog;

public interface NewPlaylistEvent {
    void onClickOK(String inputText);
}
