package com.healtheworld.nico.custom.recycleview;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by YuHung on 6/7/2015.
 * extend recycleview scroller for simple quick return patten
 */
public abstract class MyScrollListener extends RecyclerView.OnScrollListener {
    /**
     * when scroll down over 15px -> set state hide
     */
    private static final int HIDE_THRESHOLD = 15;
    /**
     * when scroll up over 5px -> show toolbar
     */
    private static final int SHOW_THRESHOLD = -5;
    private final RecycleAdapter adapter;
    private boolean hideToolBar;
    private int visibleItemCount;
    private int totalItemCount;
    private int pastVisiblesItems;
    private boolean isActiveLoadmore = false;

    /**
     * Constructor to get adapter for show load more
     *
     * @param adapter current adapter of recycle view
     */
    public MyScrollListener(RecycleAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        // select state event
        if (dy > HIDE_THRESHOLD) {
            hideToolBar = true;
        } else if (dy < SHOW_THRESHOLD) {
            hideToolBar = false;
        }

        GridLayoutManager mLayoutManager = adapter.getLayoutManager();
        visibleItemCount = mLayoutManager.getChildCount();
        totalItemCount = mLayoutManager.getItemCount();
        pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
        if (!adapter.isShowingLoadMore() && (visibleItemCount + pastVisiblesItems) >= totalItemCount && isActiveLoadmore()) {
            onLoadMore();
        }
    }

    /**
     * @return true: active loadmore, false: disable
     */
    public boolean isActiveLoadmore() {
        return isActiveLoadmore;
    }

    /**
     * set loadmore state
     *
     * @param isActiveLoadmore load more state
     */
    public void setIsActiveLoadmore(boolean isActiveLoadmore) {
        this.isActiveLoadmore = isActiveLoadmore;
    }

    /**
     * event load more
     */
    protected abstract void onLoadMore();

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (hideToolBar) {
            onHide();
        } else {
            onShow();
        }
    }

    /**
     * event hide toolbar
     */
    protected abstract void onHide();

    /**
     * event show toolbar
     */
    protected abstract void onShow();
}
