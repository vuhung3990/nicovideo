package com.healtheworld.nico.custom.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.healtheworld.nico.R;
import com.healtheworld.nico.helper.database.DatabaseUtils;

/**
 * Created by hungvu on 6/19/15.
 */
public class NewPlaylistDialog extends Dialog implements View.OnClickListener, TextWatcher {
    private final NewPlaylistEvent event;
    private Button ok;
    private Button cancel;
    private EditText input;

    public NewPlaylistDialog(Context context, NewPlaylistEvent event) {
        super(context);
        this.event = event;
    }

    public NewPlaylistDialog(Context context, int theme, NewPlaylistEvent event) {
        super(context, theme);
        this.event = event;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog_addnew);

        input = (EditText) findViewById(R.id.editText);
        input.addTextChangedListener(this);
        ok = (Button) findViewById(R.id.ok);
        cancel = (Button) findViewById(R.id.cancel);
        ok.setOnClickListener(this);
        ok.setEnabled(false);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                Cursor cursor = DatabaseUtils.getFavContainer();
                boolean isSame = false;
                if (cursor != null && cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        if (cursor.getString(cursor.getColumnIndex("name")).equalsIgnoreCase(input.getText().toString())) {
                            isSame = true;
                            break;
                        }
                        cursor.moveToNext();
                    }
                }
                if (isSame)
                    Snackbar.make(((Activity) getContext()).findViewById(android.R.id.content), R.string.same_fav_container, Snackbar.LENGTH_LONG).show();
                else {
                    dismiss();
                    event.onClickOK(input.getText().toString());
                }
                break;
            case R.id.cancel:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (count > 0) ok.setEnabled(true);
        else ok.setEnabled(false);
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}