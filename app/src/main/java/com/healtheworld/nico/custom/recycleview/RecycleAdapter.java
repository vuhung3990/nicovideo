package com.healtheworld.nico.custom.recycleview;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.healtheworld.nico.R;
import com.healtheworld.nico.custom.dialog.NewPlaylistDialog;
import com.healtheworld.nico.custom.dialog.NewPlaylistEvent;
import com.healtheworld.nico.helper.database.DatabaseUtils;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by YuHung on 5/30/2015.
 */
public class RecycleAdapter extends RecyclerView.Adapter<Holder> {
    private final Context context;
    private final AlertDialog.Builder addFavouriteDialog;
    private final ItemSubMenu callback;
    private List<RecycleItemObject> list;
    private GridLayoutManager layoutManager = null;
    private int lastPosition = 0;

    /**
     * custom recycle view adapter contructor
     * <p><b>REMEMBER: PLEASE USE <i>disableLoadmoreAndNotifyDataChanged()</i> INSTEAD OF BULD-IN FUNCTION notifyDataSetChanged()</b></p>
     *
     * @param context context of activity
     * @param list    list data
     */
    public RecycleAdapter(Context context, List<RecycleItemObject> list, ItemSubMenu callback) {
        this.list = addFavItemIntoListData(list);
        this.context = context;
        this.callback = callback;

        addFavouriteDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        addFavouriteDialog.setCancelable(true);
        addFavouriteDialog.setRecycleOnMeasureEnabled(true);
        addFavouriteDialog.setTitle(R.string.title_favourite_dialog);

        favPlayListToTop();
    }

    /**
     * make sure fav play list always on top
     */
    private void favPlayListToTop() {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isFav()) {
                RecycleItemObject temp = list.get(i);
                list.remove(i);
                list.add(0, temp);
            }
        }
        notifyDataSetChanged();
    }

    /**
     * event call when change orientation
     *
     * @param isLandscape  true: landscape, false: portrain
     * @param recyclerView recycle view
     */
    public void setLayoutOrientation(boolean isLandscape, RecyclerView recyclerView) {
        if (list != null) {
            List<RecycleItemObject> playlistSaved = new ArrayList<>();
            // set type & layout manager
            if (isLandscape) {
                for (RecycleItemObject recycleItemObject : list) {
                    // ignore load more item
                    if (recycleItemObject.getType() != RecycleItemObject.LAYOUT_ITEM_LOADMORE) {
                        if (recycleItemObject.isFav()) {
                            recycleItemObject.setType(RecycleItemObject.LAYOUT_ITEM_PORTRAIN);
                            playlistSaved.add(recycleItemObject);
                        } else
                            recycleItemObject.setType(RecycleItemObject.LAYOUT_ITEM_LANDSCAPE);
                    }
                }
                GridLayoutManager gridLayout = new GridLayoutManager(recyclerView.getContext(), 3);
                gridLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        // if fav or load more -> span = 3, else =1
                        return list.size() > position && list.get(position).isFav() || list.get(position).getType() == RecycleItemObject.LAYOUT_ITEM_LOADMORE ? 3 : 1;
                    }
                });
                layoutManager = gridLayout;
            } else {
                for (RecycleItemObject recycleItemObject : list) {
                    // ignore load more item
                    if (recycleItemObject.getType() != RecycleItemObject.LAYOUT_ITEM_LOADMORE) {
                        if (recycleItemObject.isFav()) {
                            recycleItemObject.setType(RecycleItemObject.LAYOUT_ITEM_LANDSCAPE);
                            playlistSaved.add(recycleItemObject);
                        } else
                            recycleItemObject.setType(RecycleItemObject.LAYOUT_ITEM_PORTRAIN);
                    }
                }
                layoutManager = new GridLayoutManager(recyclerView.getContext(), 1);
            }

            // sync saved playlist
            for (Iterator<RecycleItemObject> it = list.iterator(); it.hasNext(); ) {
                RecycleItemObject object = it.next();
                if (object.isFav()) it.remove();
            }
            list.addAll(0, playlistSaved);
            playlistSaved.clear();

            // scroll to last position when change orientation
            layoutManager.scrollToPosition(lastPosition);

            // set layout manager
            recyclerView.setLayoutManager(layoutManager);

            // notify data change
            notifyDataSetChanged();
        }
    }

    /**
     * @return current layout manager
     */
    public GridLayoutManager getLayoutManager() {
        return layoutManager;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == RecycleItemObject.LAYOUT_ITEM_PORTRAIN) {
            view = LayoutInflater.from(context).inflate(R.layout.item_recycle_view_main, parent, false);
        } else if (viewType == RecycleItemObject.LAYOUT_ITEM_LANDSCAPE) {
            view = LayoutInflater.from(context).inflate(R.layout.item_recycle_view_main_land, parent, false);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.item_recycle_view_main_load_more, parent, false);
        }

        return new Holder(view, callback);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        // retrieve data
        RecycleItemObject object = list.get(position);

        // ignore load more when bind data
        if (object.getType() != RecycleItemObject.LAYOUT_ITEM_LOADMORE) {
            // show popup menu
            holder.setupPopup(position, context, addFavouriteDialog, this);

            // save last position for scroll when change orientation
            lastPosition = getLayoutManager().findFirstVisibleItemPosition();

            // fill data to view
            holder.video_duration.setText(object.getDuration());
            holder.video_title.setText(object.getTitle());
            holder.video_upload_date.setText(object.getUploadDate());
            holder.video_views_count.setText(object.getViews());

            //load thumb
            Bitmap.Config config = object.isFav() ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            Picasso.with(context).load(object.getThumb()).config(config).into(holder.video_thumb);
        }
    }

    /**
     * add item to fav when select in popup menu
     *
     * @param itemPosition position of current view which is container of popup
     * @param idContainer  id of fav container
     */
    public void addFavItem(int itemPosition, int idContainer) {
        RecycleItemObject curentObject = list.get(itemPosition);

        //  check exist: if not -> insert
        if (DatabaseUtils.isExistItemInFavContainer(curentObject.getId(), idContainer)) {
            Snackbar.make(((Activity) context).findViewById(android.R.id.content), R.string.add_fav_toast_exist, Snackbar.LENGTH_LONG).show();
        } else {
            DatabaseUtils.addFavItem(curentObject.getId(), curentObject.getTitle(), curentObject.getAuthor(), curentObject.getUploadDate(), curentObject.getViews(), curentObject.getThumb(), curentObject.getDuration(), idContainer);
            Snackbar.make(((Activity) context).findViewById(android.R.id.content), R.string.add_fav_toast_success, Snackbar.LENGTH_LONG).show();

            // change layout when add to fav play list
            curentObject.setType(curentObject.getType() == RecycleItemObject.LAYOUT_ITEM_PORTRAIN ? RecycleItemObject.LAYOUT_ITEM_LANDSCAPE : RecycleItemObject.LAYOUT_ITEM_PORTRAIN);
            curentObject.setIsFav(true);
            // notify item only
            notifyItemChanged(itemPosition);

            // notify data changed
            callback.onAddPlayList();

            // add item to top
            list.remove(curentObject);
            list.add(0, curentObject);
            layoutManager.scrollToPosition(0);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * fill fav in database into list data
     *
     * @param objectList current list data
     * @return filled fav item into list data
     * @see RecycleAdapter#RecycleAdapter(Context, List, ItemSubMenu)
     * @see RecycleAdapter#disableLoadmoreAndNotifyDataChanged
     */
    public List<RecycleItemObject> addFavItemIntoListData(List<RecycleItemObject> objectList) {
        List<String> allFavItems = DatabaseUtils.getAllFavItems();
        if (objectList != null && allFavItems != null) {
            for (String videoId : allFavItems) {
                for (RecycleItemObject object : objectList) {
                    // ignore load more item
                    if (object.getType() != RecycleItemObject.LAYOUT_ITEM_LOADMORE && object.getId().equalsIgnoreCase(videoId.trim())) {
                        object.setIsFav(true);

                        //#25 set layout change
                        if (object.getType() != RecycleItemObject.LAYOUT_ITEM_LOADMORE) {
                            if (object.getType() == RecycleItemObject.LAYOUT_ITEM_LANDSCAPE) {
                                object.setType(RecycleItemObject.LAYOUT_ITEM_PORTRAIN);
                            } else if (object.getType() == RecycleItemObject.LAYOUT_ITEM_PORTRAIN) {
                                object.setType(RecycleItemObject.LAYOUT_ITEM_LANDSCAPE);
                            }
                        }
                    }
                }
            }
        }
        return objectList;
    }

    /**
     * <b>REMEMBER: CALL ME INSTEAD OF BULD-IN FUNCTION notifyDataSetChanged()</b>
     * <p>because:</p>
     * <p>---hide load more</p>
     * <p>---load fav to list</p>
     * <p>---move fav to top</p>
     *
     * @see RecycleAdapter#addFavItemIntoListData(List)
     */
    public void disableLoadmoreAndNotifyDataChanged() {
        // if showing load more -> disable
        showLoadMore(false);
        list = addFavItemIntoListData(list);
        favPlayListToTop();
    }

    public void showLoadMore(boolean showLoadmore) {
        // add load more
        if (showLoadmore) {
            if (!isShowingLoadMore()) {
                list.add(new RecycleItemObject(RecycleItemObject.LAYOUT_ITEM_LOADMORE));
                notifyItemInserted(list.size() - 1);
            }
        } else {
            // remove load more
            if (isShowingLoadMore()) {
                list.remove(list.size() - 1);
                notifyItemRemoved(list.size() - 1);
            }
        }
    }

    /**
     * @return true: if last item is load more
     */
    public boolean isShowingLoadMore() {
        return list.size() > 0 && list.get(list.size() - 1).getType() == RecycleItemObject.LAYOUT_ITEM_LOADMORE;
    }

    public interface ItemSubMenu {
        void onShare(int itemPosition);

        void onPlay(int itemPosition);

        void onAddPlayList();

        void onItemClick(int itemPosition);
    }
}

/**
 * Recycle holder object
 */
class Holder extends RecyclerView.ViewHolder {
    protected final LinearLayout item_container;
    protected final ImageView video_thumb;
    protected final TextView video_duration;
    protected final TextView video_title;
    protected final TextView video_upload_date;
    protected final TextView video_views_count;
    protected final ImageButton video_menu;
    private int itemPosition;
    private Context context;
    private AlertDialog.Builder addFavouriteDialog;
    private RecycleAdapter adapter;

    public Holder(View itemView, final RecycleAdapter.ItemSubMenu callback) {
        super(itemView);
        video_thumb = (ImageView) itemView.findViewById(R.id.video_thumb);
        video_duration = (TextView) itemView.findViewById(R.id.video_duration);
        video_title = (TextView) itemView.findViewById(R.id.video_title);
        video_upload_date = (TextView) itemView.findViewById(R.id.video_upload_date);
        video_views_count = (TextView) itemView.findViewById(R.id.video_view_count);
        video_menu = (ImageButton) itemView.findViewById(R.id.video_menu);

        // fix error when search srolling down
        if (video_menu != null) {
            video_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    generatePopupMenu(v, itemPosition, context, addFavouriteDialog, adapter, callback);
                }
            });
        }
        item_container = (LinearLayout) itemView.findViewById(R.id.item_container);
        if (item_container != null)
            item_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(itemPosition);
                }
            });
    }

    /**
     * update item status
     *
     * @param itemPosition       position
     * @param context            view's context
     * @param addFavouriteDialog custom dialog
     * @param adapter            recycle adapter
     */
    public void setupPopup(int itemPosition, Context context, AlertDialog.Builder addFavouriteDialog, RecycleAdapter adapter) {
        this.itemPosition = itemPosition;
        this.context = context;
        this.addFavouriteDialog = addFavouriteDialog;
        this.adapter = adapter;
    }

    /**
     * generate popup menu
     *
     * @param anchorView         anchor to view
     * @param itemPosition       current position of view
     * @param context            view's context
     * @param addFavouriteDialog custom dialog
     * @param adapter            recycle adapter
     */
    private void generatePopupMenu(View anchorView, final int itemPosition, final Context context, final AlertDialog.Builder addFavouriteDialog, final RecycleAdapter adapter, final RecycleAdapter.ItemSubMenu callback) {
        PopupMenu popupMenu = new PopupMenu(context, anchorView);
        popupMenu.getMenuInflater().inflate(R.menu.video_menu, popupMenu.getMenu());

        // force show icon
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // register popup event
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.video_menu_play:
                        // event play video
                        callback.onPlay(itemPosition);
                        return true;
                    case R.id.video_menu_share:
                        // event share
                        callback.onShare(itemPosition);
                        return true;
                    case R.id.video_menu_bookmark:
                        // event add video to fav
                        final Cursor cursor = DatabaseUtils.getFavContainer();
                        addFavouriteDialog.setCursor(cursor, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, final int which) {
                                dialog.dismiss();
                                if (which == 0) {
                                    // show dialog with input -> add fav container->add item fav -> show toast
                                    NewPlaylistDialog dialog1 = new NewPlaylistDialog(context, R.style.Base_Theme_AppCompat_Light_Dialog_MinWidth, new NewPlaylistEvent() {
                                        @Override
                                        public void onClickOK(String inputText) {
                                            int id = DatabaseUtils.addFavContainer(inputText);
                                            //  add item fav -> show toast
                                            cursor.moveToPosition(which);
                                            adapter.addFavItem(itemPosition, id);
                                        }
                                    });
                                    dialog1.show();
                                } else {
                                    // check exist -> add item fav -> show toast
                                    cursor.moveToPosition(which);
                                    adapter.addFavItem(itemPosition, cursor.getInt(cursor.getColumnIndex("_id")));
                                }
                            }
                        }, "name");
                        addFavouriteDialog.create().show();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }
}
