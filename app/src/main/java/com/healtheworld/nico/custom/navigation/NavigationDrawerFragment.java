package com.healtheworld.nico.custom.navigation;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.healtheworld.nico.R;
import com.healtheworld.nico.helper.common.NicoUtils;
import com.healtheworld.nico.helper.common.PicassoCircleTransform;
import com.healtheworld.nico.helper.database.AccountObject;
import com.healtheworld.nico.helper.database.DatabaseUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment implements NavigationDrawerCallbacks {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerList;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    private ImageView avatar;
    private TextView userName;
    private TextView userEmail;
    private ProfileCallback mProfileCallback;
    private List<NavigationItem> navigationItems;
    private NavigationDrawerAdapter adapter;
    private String mCurrentCategory = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mDrawerList = (RecyclerView) view.findViewById(R.id.drawerList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mDrawerList.setLayoutManager(layoutManager);
        mDrawerList.setHasFixedSize(true);

        navigationItems = new ArrayList<>();

        generateNavigationItems();
        adapter = new NavigationDrawerAdapter(navigationItems);
        adapter.setNavigationDrawerCallbacks(this);
        mDrawerList.setAdapter(adapter);

        // default select category all
        mCurrentCategory = NicoUtils.CATEGORY_ALL;
        selectItem(navigationItems.size() - 7, mCurrentCategory, false, true);

        AccountObject account = DatabaseUtils.getAccountSaved();

        avatar = (ImageView) view.findViewById(R.id.avatar);
        userName = (TextView) view.findViewById(R.id.ac_name);
        userEmail = (TextView) view.findViewById(R.id.ac_email);

        // load avatar if exist
        if (account != null) {
            if (account.getAvatar() != null)
                Picasso.with(getActivity()).load(account.getAvatar()).transform(new PicassoCircleTransform()).into(avatar);
            if (account.getName() != null) userName.setText(account.getName());
            if (account.getEmail() != null) userEmail.setText(account.getEmail());
        }

        // set up profile event
        RelativeLayout profileContainer = (RelativeLayout) view.findViewById(R.id.profile_container);
        profileContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProfileCallback != null) {
                    if (DatabaseUtils.getAccountSaved() != null) {
                        mProfileCallback.onProfileLogout();
                    } else {
                        mProfileCallback.onProfileLogin();
                    }
                }
            }
        });
        return view;
    }

    /**
     * re generate all navigation items
     */
    public void generateNavigationItems() {
        List<NavigationItem> items = new ArrayList<>();
        // Label playlist
        Cursor cursor = DatabaseUtils.getFavContainer();
        // i did add a jungle value at top of cursor
        if (cursor.getCount() > 1) {
            items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_LABEL, getString(R.string.playlist_label), -1, NavigationItem.NO_HAVE_SUB_MENU));
        }
        // generate playlist from db
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                if (cursor.getInt(cursor.getColumnIndex("_id")) > -1) // i did add a jungle value at top of cursor
                    items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, cursor.getString(cursor.getColumnIndex("name")), DatabaseUtils.countItemInFavContainer(cursor.getInt(cursor.getColumnIndex("_id"))), NavigationItem.NO_HAVE_SUB_MENU));
                cursor.moveToNext();
            }
        }
        cursor.close();

        // Label categories
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_LABEL, getString(R.string.category_label), -1, NavigationItem.NO_HAVE_SUB_MENU));
        // add all categories
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, getString(R.string.category_all), -1, NavigationItem.NO_HAVE_SUB_MENU));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, getString(R.string.category_entertaiment_music), -1, R.menu.category_entertaiment_music));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, getString(R.string.category_life_general_sport), -1, R.menu.category_life_general_sport));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, getString(R.string.category_politics), -1, NavigationItem.NO_HAVE_SUB_MENU));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, getString(R.string.category_science_tech), -1, R.menu.category_science_tech));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, getString(R.string.category_anime_art_game), -1, R.menu.category_anime_art_games));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_VIEW_TYPE_ITEM, getString(R.string.category_other), -1, R.menu.category_others_all));

        navigationItems.clear();
        navigationItems.addAll(items);
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    /**
     * save current item for set background later (save current position)
     *
     * @return NavigationItem
     * @see #setCurrentSelectedPosition(NavigationItem)
     */
    public NavigationItem getCurrentItem() {
        return navigationItems.size() > mCurrentSelectedPosition ? navigationItems.get(mCurrentSelectedPosition) : null;
    }

    /**
     * find previous position and update selected item
     *
     * @see #getCurrentItem()
     */
    public void setCurrentSelectedPosition(NavigationItem currentItem) {
        if (currentItem != null) {
            mCurrentSelectedPosition = navigationItems.indexOf(currentItem);
            for (int i = 0; i < navigationItems.size(); i++) {
                if (currentItem.getText().equalsIgnoreCase(navigationItems.get(i).getText())
                        && currentItem.getShowMenuButton() == navigationItems.get(i).getShowMenuButton()
                        && currentItem.getCount() == navigationItems.get(i).getCount()
                        && currentItem.getType() == navigationItems.get(i).getType()
                        && currentItem.getResSubMenu() == navigationItems.get(i).getResSubMenu()) {

                    NavigationItem navigationItem = navigationItems.get(i);

                    // mCurrentSelectedPosition > 0 && mCurrentSelectedPosition < navigationItem.getCount() - 7 : from local [ 0 < pos < total - 7 (7 category) ]
                    selectItem(i, mCurrentCategory, mCurrentSelectedPosition > 0 && mCurrentSelectedPosition < navigationItems.size() - 7, false);
                    break;
                }
            }
        }
    }

    /**
     * set profile event
     *
     * @param callback event
     */
    public void setProfileListener(ProfileCallback callback) {
        mProfileCallback = callback;
    }

    /**
     * set account info
     *
     * @param email     login id
     * @param name      user's name
     * @param avatarUrl thumb avatar url
     */
    public void setAccountInfo(String email, String name, String avatarUrl) {
        if (email != null && name != null && avatarUrl != null) {
            Picasso.with(getActivity()).load(avatarUrl).transform(new PicassoCircleTransform()).into(avatar);
            userName.setText(name);
            userEmail.setText(email);
        }
    }

    /**
     * clear account in db and reset profile views
     */
    public void resetAccountInfo() {
        // clear db
        DatabaseUtils.deleteAllAccountSaved();

        // reset view
        avatar.setImageResource(R.drawable.shape_avatar);
        userName.setText(getString(R.string.name_guest));
        userEmail.setText(getString(R.string.email_guest));
    }

    /**
     * @return true: drawer opened
     */
    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return mActionBarDrawerToggle;
    }

//    public DrawerLayout getDrawerLayout() {
//        return mDrawerLayout;
//    }

    @Override
    public void onNavigationDrawerItemSelected(int position, String category, boolean isFromLocal, boolean reloadData) {
        mCurrentCategory = category;
        selectItem(position, category, position > 0 && position < navigationItems.size() - 7, reloadData);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     * @param toolbar      The Toolbar of the activity.
     */
    public void setup(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = (View) getActivity().findViewById(fragmentId).getParent();
        mDrawerLayout = drawerLayout;

        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.myPrimaryDarkColor));

        mActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) return;

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) return;
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }
                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    /**
     * select item normal way
     *
     * @param position    which item selected
     * @param category    category string for load from data nico server
     * @param isFromLocal true if position which generated from database
     * @param reload      true: if you want refresh data
     */
    private void selectItem(int position, String category, boolean isFromLocal, boolean reload) {
        mCurrentSelectedPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position, category, isFromLocal, reload);
        }
        ((NavigationDrawerAdapter) mDrawerList.getAdapter()).selectPosition(position);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(mFragmentContainerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    public interface ProfileCallback {
        void onProfileLogout();

        void onProfileLogin();
    }
}