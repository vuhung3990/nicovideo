package com.healtheworld.nico.custom.recycleview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by YuHung on 6/10/2015.
 */
public class AutoScaleThumb extends ImageView {
    public AutoScaleThumb(Context context) {
        super(context);
    }

    public AutoScaleThumb(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoScaleThumb(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * set thumb with 4:3
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, (int) (widthMeasureSpec * 0.5625));
    }
}
