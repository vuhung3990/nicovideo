package com.healtheworld.nico.custom.recycleview;

/**
 * Created by YuHung on 5/30/2015.
 */
public class RecycleItemObject {
    public static int LAYOUT_ITEM_PORTRAIN = 0;
    public static int LAYOUT_ITEM_LANDSCAPE = 1;
    public static int LAYOUT_ITEM_LOADMORE = 2;
    private int type;
    private String id;
    private String title;
    private String author;
    private String uploadDate;
    private String views;
    private String thumb;
    private String duration;
    private String description;
    /**
     * this is hidden from constructor
     */
    private boolean isFav;

    /**
     * constructor for load more item
     *
     * @param type Layout type ( {@link #LAYOUT_ITEM_PORTRAIN}, {@link #LAYOUT_ITEM_LANDSCAPE}, {@link #LAYOUT_ITEM_LOADMORE})
     */
    public RecycleItemObject(int type) {
        this.type = type;
    }

    /**
     * Recycleview item object
     *
     * @param type        Layout type ( {@link #LAYOUT_ITEM_PORTRAIN}, {@link #LAYOUT_ITEM_LANDSCAPE}, {@link #LAYOUT_ITEM_LOADMORE})
     * @param id          video id
     * @param title       video name
     * @param author      uploader
     * @param uploadDate  upload time
     * @param views       view count
     * @param thumb       link thumb
     * @param duration    video duration (hh:mm:ss)
     * @param description video description
     */
    public RecycleItemObject(int type, String id, String title, String author, String uploadDate, String views, String thumb, String duration, String description) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.uploadDate = uploadDate;
        this.views = views;
        this.thumb = thumb;
        this.duration = duration;
        this.type = type;
        isFav = false;
        this.description = description;
    }

    /**
     * @return video description
     */
    public String getDescription() {
        return description;
    }

    /**
     * set video description
     *
     * @param description video description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return get video id
     */
    public String getId() {
        return id;
    }

    /**
     * set video id
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return get video name
     */
    public String getTitle() {
        return title;
    }

    /**
     * set video name
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return get uploader
     */
    public String getAuthor() {
        return author;
    }

    /**
     * set uploader
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return get video upload's date
     */
    public String getUploadDate() {
        return uploadDate;
    }

    /**
     * set video upload date
     *
     * @param uploadDate
     */
    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    /**
     * @return get view count
     */
    public String getViews() {
        return views;
    }

    /**
     * set view count
     *
     * @param views
     */
    public void setViews(String views) {
        this.views = views;
    }

    /**
     * @return get video link thumb
     */
    public String getThumb() {
        return thumb;
    }

    /**
     * set video link thumb
     *
     * @param thumb
     */
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    /**
     * @return get video duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * get video duration
     *
     * @param duration
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return get layout type of item
     */
    public int getType() {
        return type;
    }

    /**
     * set layout type of item
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return check video is fav
     */
    public boolean isFav() {
        return isFav;
    }

    /**
     * set fav
     *
     * @param isFav
     */
    public void setIsFav(boolean isFav) {
        this.isFav = isFav;
    }
}