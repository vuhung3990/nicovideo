package com.healtheworld.nico.custom.navigation;


/**
 * Created by poliveira on 24/10/2014.
 */
public class NavigationItem {
    public static int NAVIGATION_VIEW_TYPE_LABEL = 0;
    public static int NAVIGATION_VIEW_TYPE_ITEM = 1;
    public static int NO_HAVE_SUB_MENU = -1;
    private boolean mShowMenuButton;
    private int mCount;
    private int mType;
    private String mText;
    private int mResSubMenu;

    /**
     * Constructor
     *
     * @param type       {@link NavigationItem#NAVIGATION_VIEW_TYPE_ITEM}, {@link #NAVIGATION_VIEW_TYPE_LABEL}
     * @param text       item text
     * @param count      item count ( if -1 nothing show )
     * @param resSubMenu res sub menu, if not please set -1
     */
    public NavigationItem(int type, String text, int count, int resSubMenu) {
        mText = text;
        mType = type;
        mCount = count;
        mShowMenuButton = resSubMenu != NO_HAVE_SUB_MENU;
        mResSubMenu = resSubMenu;
    }

    /**
     * @return menu id from res
     */
    public int getResSubMenu() {
        return getShowMenuButton() ? mResSubMenu : -1;
    }

    /**
     * set menu id
     *
     * @param mResSubMenu menu id from res
     */
    public void setResSubMenu(int mResSubMenu) {
        this.mResSubMenu = getShowMenuButton() ? mResSubMenu : -1;
    }

    /**
     * @return get item count ( if -1 nothing show )
     */
    public int getCount() {
        return mCount;
    }

    /**
     * set item count
     *
     * @param mCount item count
     */
    public void setCount(int mCount) {
        this.mCount = mCount;
    }

    /**
     * @return {@link NavigationItem#NAVIGATION_VIEW_TYPE_ITEM}, {@link #NAVIGATION_VIEW_TYPE_LABEL} ...
     */
    public int getType() {
        return mType;
    }

    /**
     * set item type
     *
     * @param mType {@link NavigationItem#NAVIGATION_VIEW_TYPE_ITEM}, {@link #NAVIGATION_VIEW_TYPE_LABEL} ...
     */
    public void setType(int mType) {
        this.mType = mType;
    }

    /**
     * @return get label text
     */
    public String getText() {
        return mText;
    }

    /**
     * set label text
     *
     * @param mText label text
     */
    public void setText(String mText) {
        this.mText = mText;
    }

    /**
     * @return true: have menu button
     */
    public boolean getShowMenuButton() {
        return mShowMenuButton;
    }

    /**
     * set menu button visible
     *
     * @param mShowMenuButton true: have menu button
     */
    public void setShowMenuButton(boolean mShowMenuButton) {
        this.mShowMenuButton = mShowMenuButton;
    }
}