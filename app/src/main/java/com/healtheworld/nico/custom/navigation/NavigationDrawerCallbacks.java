package com.healtheworld.nico.custom.navigation;

import com.healtheworld.nico.helper.common.NicoUtils;

public interface NavigationDrawerCallbacks {
    /**
     * set callback when selected item navigation
     *
     * @param position    position in list navigation
     * @param category    {@link NicoUtils#CATEGORY_ALL}, {@link NicoUtils#CATEGORY_VOCALOID} ...
     * @param isFromLocal true: if item generated from database
     * @param reloadData  true if you want to get refresh data
     */
    void onNavigationDrawerItemSelected(int position, String category, boolean isFromLocal, boolean reloadData);
}
