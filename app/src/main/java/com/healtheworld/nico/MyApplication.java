package com.healtheworld.nico;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.healtheworld.nico.config.Setting;
import com.healtheworld.nico.helper.database.DatabaseUtils;

/**
 * Created by ForestMan on 7/22/2015.
 */
public class MyApplication extends Application {
    private InterstitialAd mInterstitialAd;
    private int mAdsCountDown;
    private SharedPreferences pref;

    @Override
    public void onCreate() {
        super.onCreate();
        // load ads
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ads_id));
        AdRequest adRequest = new AdRequest.Builder()
                .addKeyword("game")
                .addKeyword("video")
                .addKeyword("nico")
                .addKeyword("japanese")
                .addKeyword("anime")
                .addKeyword("mario")
                .addKeyword("bike")
                .build();
        mInterstitialAd.loadAd(adRequest);

        // current ads countdown
        pref = getSharedPreferences(getString(R.string.ads_count_down), MODE_PRIVATE);
        mAdsCountDown = pref.getInt(getString(R.string.ads_count_down_key), -1);

        // init database only one time
        DatabaseUtils.initial(this);
    }

    /**
     * show ads if loaded
     *
     * @param listener ads state listener
     */
    public void showInterstitialAds(AdListener listener) {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.setAdListener(listener);
            mInterstitialAd.show();
        } else {
            listener.onAdClosed();
        }
    }

    /**
     * update countdown for show ads
     */
    public void incrementAdsCountDown() {
        mAdsCountDown = mAdsCountDown > Setting.getAdsCountDown() ? -1 : mAdsCountDown + 1;
        pref.edit().putInt(getString(R.string.ads_count_down_key), mAdsCountDown).apply();
    }

    /**
     * @return countdown for show ads
     */
    public boolean isEndAdsCountDown() {
        return mAdsCountDown == Setting.getAdsCountDown();
    }
}