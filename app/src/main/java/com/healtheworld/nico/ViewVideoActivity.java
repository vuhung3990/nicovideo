package com.healtheworld.nico;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.healtheworld.myvideocustomize.VideoView;
import com.healtheworld.nico.config.Setting;
import com.healtheworld.nico.custom.recycleview.RecycleAdapter;
import com.healtheworld.nico.custom.recycleview.RecycleItemObject;
import com.healtheworld.nico.helper.common.NicoUtils;
import com.healtheworld.nico.helper.common.Utils;
import com.healtheworld.nico.helper.database.AccountObject;
import com.healtheworld.nico.helper.database.DatabaseUtils;
import com.healtheworld.nico.helper.notification.NewMessageNotification;

import org.apache.http.Header;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by myshadow on 30/06/2015.
 */
public class ViewVideoActivity extends Activity implements View.OnTouchListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener, RecycleAdapter.ItemSubMenu, VideoView.VideoSizeChangeListener {
    /**
     * bundle key to get video id from main
     */
    public static final String KEY_VIDEO_ID = "videoID";
    /**
     * bundle key to get video title from main
     */
    public static final String KEY_VIDEO_TITLE = "videoTitle";
    // 3 second
    private static final long CONTROLLER_DELAY = 3000;
    // 1s
    private static final long UPDATE_SEEKBAR_DELAY = 1000;
    private VideoView mVideoView;
    private LinearLayout mCenterControlContainer;
    private RelativeLayout mTopControlContainer;
    private RelativeLayout mBottomControlContainer;

    /**
     * save controller visible state
     */
    private boolean mControllerVisibleState;
    private FrameLayout mVideo_container;
    private Button mControllPrev;
    private Button mControllPlay;
    private Button mControllNext;
    private SeekBar mControllSeek;
    private Button mControllInfo;
    private Button mControllShare;
    private TextView mControllTotalTime;
    private TextView mControllCurrentTime;
    private Button mControllRepeat;
    private Button mControllFullscreen;
    private Button mControllAdd;
    private Handler mHandleHide;
    private Runnable mHideEvent;
    private Handler mHandleUpdateSeekBar;
    private Runnable mUpdateSeekBarEven;
    private int mSeekTo;
    private RecyclerView mRelativeVideos;
    private List<RecycleItemObject> listData;
    private Context mContext;
    private RecycleAdapter adapter;
    private MediaPlayer.OnInfoListener onInfoListener;
    private FrameLayout mProgress;
    private TextView mTitle;
    private String mVideoTitle;
    private String mVideoId;
    /**
     * true when press back
     */
    private boolean isPressBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide status bar by default
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_video);

        // increment countdown
        ((MyApplication) getApplicationContext()).incrementAdsCountDown();

        mContext = this;
        Utils.toggleFullscreen(this, true);

        // controllers container for show/hide toggle
        mCenterControlContainer = (LinearLayout) findViewById(R.id.center_control_container);
        mTopControlContainer = (RelativeLayout) findViewById(R.id.top_control_container);
        mBottomControlContainer = (RelativeLayout) findViewById(R.id.bottom_control_container);

        // play progress
        mProgress = (FrameLayout) findViewById(R.id.play_progress);

        // video view container contain video and controllers
        mVideo_container = (FrameLayout) findViewById(R.id.videoview_container);
        mVideo_container.setOnTouchListener(this);

        // controllers
        mControllPrev = (Button) findViewById(R.id.prev);
        mControllPlay = (Button) findViewById(R.id.play);
        mControllNext = (Button) findViewById(R.id.next);
        mControllInfo = (Button) findViewById(R.id.info);
        mControllShare = (Button) findViewById(R.id.share);
        mControllAdd = (Button) findViewById(R.id.add);
        mControllFullscreen = (Button) findViewById(R.id.fullscreen);
        mControllRepeat = (Button) findViewById(R.id.repeat);
        mControllCurrentTime = (TextView) findViewById(R.id.current_time);
        mControllTotalTime = (TextView) findViewById(R.id.total_time);
        mControllSeek = (SeekBar) findViewById(R.id.seek);

        mControllPrev.setOnClickListener(this);
        mControllPlay.setOnClickListener(this);
        mControllNext.setOnClickListener(this);
        mControllInfo.setOnClickListener(this);
        mControllShare.setOnClickListener(this);
        mControllAdd.setOnClickListener(this);
        mControllFullscreen.setOnClickListener(this);
        mControllRepeat.setOnClickListener(this);

        mVideoTitle = getIntent().getStringExtra(KEY_VIDEO_TITLE);
        mTitle = (TextView) findViewById(R.id.title_video);
        mTitle.setText(mVideoTitle);

        // video view
        mVideoView = (VideoView) findViewById(R.id.video_view);
        mVideoView.setVideoSizeChangeListener(this);

        // default hide controller
        toggleShowController(false);

        mControllSeek.setOnSeekBarChangeListener(this);

        // event show/hide controller
        mHandleHide = new Handler();
        mHideEvent = new Runnable() {
            @Override
            public void run() {
                // only hide
                if (mControllerVisibleState) toggleShowController(false);
            }
        };

        // handle update seekbar
        mHandleUpdateSeekBar = new Handler();
        mUpdateSeekBarEven = new Runnable() {
            @Override
            public void run() {
                // only update while playing
                mControllSeek.setProgress(mVideoView.getCurrentPosition());

                // loop update
                mHandleUpdateSeekBar.postDelayed(mUpdateSeekBarEven, UPDATE_SEEKBAR_DELAY);
            }
        };

        // can't get video id -> finish
        mVideoId = getIntent().getStringExtra(KEY_VIDEO_ID);
        if (mVideoId == null) finish();

        //  show loading in video player
        AccountObject account = DatabaseUtils.getAccountSaved();
        NicoUtils.getNicoUrlAndCookie(this, account.getEmail(), account.getPassword(), mVideoId, new NicoUtils.getVideoCallback() {
            @Override
            public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("fail", responseString);
            }

            @SuppressLint("NewApi")
            @Override
            public void onSuccess(String userSession, String nicoHistory, String url) throws UnsupportedEncodingException {
//                Log.d("debug", "uss: " + userSession + ", nh:" + nicoHistory + ", url: " + url);
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Cookie", "nicohistory=" + nicoHistory);
                mVideoView.setVideoURI(Uri.parse(url), headers);
            }

            @Override
            public void onGetVideoIDFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(Setting.getLogTag(), "onGetVideoIDFail: " + responseString);
            }

            @Override
            public void onLoginFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(Setting.getLogTag(), "onLoginFail: " + responseString);
            }
        });

        // video info listener
        onInfoListener = new MediaPlayer.OnInfoListener() {

            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                        // show buffering
                        mProgress.setVisibility(View.VISIBLE);
                        toggleShowController(false);
                        return true;
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                        // Dimiss buffering
                        mProgress.setVisibility(View.GONE);
                        // hide controller
                        toggleShowController(false);
                        return true;
                }
                return false;
            }
        };

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnInfoListener(onInfoListener);

                // update seekbar
                mControllSeek.setMax(mp.getDuration());
                mControllTotalTime.setText(Utils.convertSecondsToHMmSs(mp.getDuration()));

                // update seekbar when start
                mHandleUpdateSeekBar.postDelayed(mUpdateSeekBarEven, UPDATE_SEEKBAR_DELAY);

                // hide progress by default
                mProgress.setVisibility(View.GONE);
                mp.start();
            }
        });

        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mControllSeek.setProgress(mp.getDuration());

                // remove update handle
                mHandleUpdateSeekBar.removeCallbacks(mUpdateSeekBarEven);
            }
        });

        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                //TODO: try again
                return true;
            }
        });

        mRelativeVideos = (RecyclerView) findViewById(R.id.relative_videos_list);
        mRelativeVideos.setHasFixedSize(true);

        listData = new ArrayList<>();
        adapter = new RecycleAdapter(mContext, listData, this);

        NicoUtils.getRelativeVideos(mVideoId, 1, listData, new NicoUtils.getRelativeVideosCallback() {
            @Override
            public void onFailure(int statusCode, String responseString, Throwable throwable) {
                // hide loading content
                findViewById(R.id.content_loading).setVisibility(View.GONE);
                Log.e(Setting.getLogTag(), "StatusCode: " + statusCode + ", " + responseString, throwable);
            }

            @Override
            public void onSuccess() {
                // hide loading content
                findViewById(R.id.content_loading).setVisibility(View.GONE);
                adapter.disableLoadmoreAndNotifyDataChanged();
            }
        });

        // init layout orientation
        adapter.setLayoutOrientation(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE, mRelativeVideos);
        mRelativeVideos.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);

        // notify play in background
        if (!isPressBack && mVideoView != null && mVideoView.isPlaying()) {
            NewMessageNotification.notify(this, mVideoTitle, NicoUtils.BASE_VIDEO_LINK + mVideoId);
        } else {
            // cancel notify
            NewMessageNotification.cancel(this);
            isPressBack = false;
        }
    }

    /**
     * toggle show/hide controller when touch on video view
     *
     * @param isShow true: show, false: hide
     */
    private void toggleShowController(boolean isShow) {
        int v = isShow ? View.VISIBLE : View.GONE;
        mCenterControlContainer.setVisibility(v);

        // TODO: 17-Sep-15 disable top controller template
//        mTopControlContainer.setVisibility(v);

        mTitle.setVisibility(v);
        mBottomControlContainer.setVisibility(v);

        // update state for re-use later
        mControllerVisibleState = isShow;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (v.getId() == mVideo_container.getId() && mProgress.getVisibility() == View.GONE) {
                toggleShowController(!mControllerVisibleState);
                resetAutoHideCotroller();
            }
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prev:
                resetAutoHideCotroller();
                break;
            case R.id.play:
                resetAutoHideCotroller();
                // change icon
                togglePlayback();
                break;
            case R.id.next:
                resetAutoHideCotroller();
                break;
            case R.id.info:
                resetAutoHideCotroller();
                break;
            case R.id.share:
                resetAutoHideCotroller();
                break;
            case R.id.add:
                resetAutoHideCotroller();
                break;
            case R.id.fullscreen:
                resetAutoHideCotroller();
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    // set landscape and change icon
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    mControllFullscreen.setBackgroundResource(R.drawable.ic_fullscreen_exit);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                    mControllFullscreen.setBackgroundResource(R.drawable.ic_fullscreen);
                }
                break;
            case R.id.repeat:
                resetAutoHideCotroller();
                break;
        }
    }

    /**
     * toggle play/pause
     */
    private void togglePlayback() {
        if (mVideoView.isPlaying()) {
            mControllPlay.setBackgroundResource(R.drawable.ic_play);

            // stop update seek bar
            mHandleUpdateSeekBar.removeCallbacks(mUpdateSeekBarEven);
            mVideoView.pause();
        } else {
            mControllPlay.setBackgroundResource(R.drawable.ic_pause);

            // update seek bar again
            mHandleUpdateSeekBar.postDelayed(mUpdateSeekBarEven, UPDATE_SEEKBAR_DELAY);
            mVideoView.start();
        }
    }

    private void resetAutoHideCotroller() {
        mHandleHide.removeCallbacks(mHideEvent);
        mHandleHide.postDelayed(mHideEvent, CONTROLLER_DELAY);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        seekBar.setProgress(progress);
        //seek video to current progress
        if (fromUser) {
            mSeekTo = progress;
        }

        // when change -> update current
        mControllCurrentTime.setText(Utils.convertSecondsToHMmSs(progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // do not auto hide and update seek bar when drag
        mHandleHide.removeCallbacks(mHideEvent);
        mHandleUpdateSeekBar.removeCallbacks(mUpdateSeekBarEven);

        // reset value
        mSeekTo = -1;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandleHide.postDelayed(mHideEvent, CONTROLLER_DELAY);
        mHandleUpdateSeekBar.postDelayed(mUpdateSeekBarEven, UPDATE_SEEKBAR_DELAY);

        // only seek video when cancel drag
        if (mSeekTo > -1) {
            mVideoView.seekTo(mSeekTo);
        }
    }

    @Override
    public void onShare(int itemPosition) {
        Utils.shareTextUrl(this, NicoUtils.BASE_VIDEO_LINK + listData.get(itemPosition).getId());
    }

    @Override
    public void onPlay(int itemPosition) {
        // restart activity with new play id
        Intent intent = getIntent();
        intent.putExtra(ViewVideoActivity.KEY_VIDEO_ID, listData.get(itemPosition).getId());
        intent.putExtra(ViewVideoActivity.KEY_VIDEO_TITLE, listData.get(itemPosition).getTitle());
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    @Override
    public void onAddPlayList() {
    }

    @Override
    public void onItemClick(int itemPosition) {
        onPlay(itemPosition);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isPressBack = true;
    }

    @Override
    public void onFullScreen() {
        // hide system ui like: navigation bar, status bar...
    }

    @Override
    public void onNormalSize() {
        // show system ui
    }
}
