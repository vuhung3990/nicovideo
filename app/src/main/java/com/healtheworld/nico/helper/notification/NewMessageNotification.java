package com.healtheworld.nico.helper.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.healtheworld.nico.R;
import com.healtheworld.nico.ViewVideoActivity;

/**
 * Helper class for showing and canceling new message
 * notifications.
 * <p/>
 * This class makes heavy use of the {@link NotificationCompat.Builder} helper
 * class to create notifications in a backward-compatible way.
 */
public class NewMessageNotification {
    /**
     * The unique identifier for this type of notification.
     */
    private static final String NOTIFICATION_TAG = "playback_ground";

    /**
     * Shows the notification, or updates a previously shown notification of
     * this type, with the given parameters.
     * <p/>
     * TODO: Customize this method's arguments to present relevant content in
     * the notification.
     * <p/>
     * TODO: Customize the contents of this method to tweak the behavior and
     * presentation of new message notifications. Make
     * sure to follow the
     * <a href="https://developer.android.com/design/patterns/notifications.html">
     * Notification design guidelines</a> when doing so.
     *
     * @param videoTitle Notify title
     * @param urlToShare url video to share
     * @see #cancel(Context)
     */
    public static void notify(final Context context, final String videoTitle, String urlToShare) {
        final Resources res = context.getResources();

        // This image is used as the notification's large icon (thumbnail).
//        final Bitmap picture = BitmapFactory.decodeResource(res, R.mipmap.ic_launcher);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(videoTitle)
                .setContentText(res.getString(R.string.play_in_background))
                        // All fields below this line are optional.

                        // Use a default priority (recognized on devices running Android
                        // 4.1 or later)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Provide a large icon, shown with the notification in the
                        // notification drawer on devices running Android 3.0 or later.
//                .setLargeIcon(picture)

                        // Set ticker text (preview) information for this notification.
                .setTicker(res.getString(R.string.play_in_background))

                        // Set the pending intent to be initiated when the user touches
                        // the notification.
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                0,
                                new Intent(context, ViewVideoActivity.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                                PendingIntent.FLAG_CANCEL_CURRENT))

                        // action play/pause
//                .addAction(
//                        android.R.drawable.ic_media_play,
//                        res.getString(R.string.action_play),
//                        PendingIntent.getBroadcast(
//                                context,
//                                0,
//                                new Intent(MediaBroadcast.ACTION_MEDIA_TOGGLE),
//                                PendingIntent.FLAG_UPDATE_CURRENT
//                        ))
//
//                        // action share
//                .addAction(
//                        R.drawable.ic_action_stat_share,
//                        res.getString(R.string.action_share),
//                        PendingIntent.getActivity(
//                                context,
//                                0,
//                                Intent.createChooser(new Intent(Intent.ACTION_SEND)
//                                        .setType("text/plain")
//                                        .putExtra(Intent.EXTRA_TEXT, urlToShare), res.getString(R.string.share_title)),
//                                PendingIntent.FLAG_UPDATE_CURRENT))
//
//                        // action close
//                .addAction(
//                        android.R.drawable.ic_menu_close_clear_cancel,
//                        res.getString(R.string.negative_label_btn),
//                        PendingIntent.getBroadcast(
//                                context,
//                                0,
//                                new Intent(MediaBroadcast.ACTION_MEDIA_CLOSE),
//                                PendingIntent.FLAG_CANCEL_CURRENT)
//                )
                        // Automatically dismiss the notification when it is touched.
                .setAutoCancel(true);

        notify(context, builder.build());
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.notify(NOTIFICATION_TAG, 0, notification);
        } else {
            nm.notify(NOTIFICATION_TAG.hashCode(), notification);
        }
    }

    /**
     * Cancels any notifications of this type previously shown using
     * {@link #notify(Context, Notification)}.
     */
    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.cancel(NOTIFICATION_TAG, 0);
        } else {
            nm.cancel(NOTIFICATION_TAG.hashCode());
        }
    }
}
