package com.healtheworld.nico.helper.common;

import android.content.Context;
import android.content.res.Configuration;

import com.google.gson.Gson;
import com.healtheworld.nico.custom.recycleview.RecycleItemObject;
import com.healtheworld.nico.helper.database.DatabaseUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.cookie.Cookie;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by YuHung on 5/11/2015.
 * require: INTERNET permission, android-async-http library (tested on version 1.4.7)
 */
public class NicoUtils {

    /**
     * All Categories
     */
    public static final String CATEGORY_ALL = "all";
    /**
     * Entertainment・Music Categories
     */
    public static final String CATEGORY_ENTERTAIMENT_MUSIC = "g_ent2";
    public static final String CATEGORY_ENTERTAIMENT = "ent";
    public static final String CATEGORY_MUSIC = "music";
    public static final String CATEGORY_SANG_IT = "sing";
    public static final String CATEGORY_PLAYED_IT = "play";
    public static final String CATEGORY_DANCED_IT = "dance";
    public static final String CATEGORY_VOCALOID = "vocaloid";
    public static final String CATEGORY_NICO_INDIES = "nicoindies";
    /**
     * Life・General・Sports Categories
     */
    public static final String CATEGORY_LIFE_GENERAL_SPORT = "g_life2";
    public static final String CATEGORY_ANIMALS = "animal";
    public static final String CATEGORY_COOKING = "cooking";
    public static final String CATEGORY_NATURE = "nature";
    public static final String CATEGORY_TRAVELING = "travel";
    public static final String CATEGORY_SPORTS = "sport";
    public static final String CATEGORY_NICO_LECTURE = "lecture";
    public static final String CATEGORY_VEHICLES = "drive";
    public static final String CATEGORY_HISTORY = "history";
    /**
     * Politics Categories
     */
    public static final String CATEGORY_POLITICS = "g_politics";
    /**
     * Science・Tech Categories
     */
    public static final String CATEGORY_SCIENCE_TECH = "g_tech";
    public static final String CATEGORY_SCIENCE = "science";
    public static final String CATEGORY_NICO_TECH = "tech";
    public static final String CATEGORY_NICO_CRAFTS = "handcraft";
    public static final String CATEGORY_MADE_IT = "make";
    /**
     * Anime・Art・Games Categories
     */
    public static final String CATEGORY_ANIME_ART_GAMES = "g_culture2";
    public static final String CATEGORY_ANIME = "anime";
    public static final String CATEGORY_GAMES = "game";
    public static final String CATEGORY_TOUHOU = "toho";
    public static final String CATEGORY_IDOLMASTER = "imas";
    public static final String CATEGORY_RADIO = "radio";
    public static final String CATEGORY_DREW_IT = "draw";
    /**
     * Others Categories
     */
    public static final String CATEGORY_OTHERS_ALL = "g_other";
    public static final String CATEGORY_THAT_THING = "are";
    public static final String CATEGORY_DIARY = "diary";
    public static final String CATEGORY_OTHERS = "other";
    public static final String LANG_ENG = "en-us";
    public static final String LANG_JAP = "ja-jp";
    public static final String BASE_VIDEO_LINK = DatabaseUtils.decrypt("aHR0cDovL3d3dy5uaWNvdmlkZW8uanAvd2F0Y2gvc2RmZ2hpdW8");
    private static final String USER_AGENT = "Mozilla/4.0 (compatible; MSIE 7.0b; Windows 98)";
    private static final String LOGIN_PATH = DatabaseUtils.decrypt("aHR0cHM6Ly9zZWN1cmUubmljb3ZpZGVvLmpwL3NlY3VyZS9sb2dpbj9zaXRlPXNwbmljb25pY29zZGZnaGl1bw");
    private static String GET_FLV_PATH = DatabaseUtils.decrypt("aHR0cDovL2ZsYXBpLm5pY292aWRlby5qcC9hcGkvZ2V0Zmx2L3NkZmdoaXVv");
    private static String SEARCH_PATH = DatabaseUtils.decrypt("aHR0cDovL3JpYXBpLm5pY292aWRlby5qcC9hcGkvc2VhcmNoL3NlYXJjaD9zb3J0PW4mb3JkZXI9ZCZwYWdlPXNkZmdoaXVv");
    private static String RSS_PATH = DatabaseUtils.decrypt("aHR0cDovL3d3dy5uaWNvdmlkZW8uanAvcmFua2luZy9mYXYvZGFpbHkvc2RmZ2hpdW8");
    private static String MAIN_PATH = DatabaseUtils.decrypt("aHR0cDovL3d3dy5uaWNvdmlkZW8uanAvc2RmZ2hpdW8");
    private static String USER_ICON_PATH = DatabaseUtils.decrypt("aHR0cDovL3VzZXJpY29uLm5pbWcuanAvdXNlcmljb24vcy9zZGZnaGl1bw");
    private static String RELATION_PATH = DatabaseUtils.decrypt("aHR0cDovL2ZsYXBpLm5pY292aWRlby5qcC9hcGkvZ2V0cmVsYXRpb25zZGZnaGl1bw");

    /**
     * login nico video
     *
     * @param context
     * @param email
     * @param password
     * @param callback
     * @see #getNicoUrlAndCookie(Context, String, String, String, getVideoCallback)
     * @see #searchVideo(Context, int, String, String, String, searchVideoCallback)
     */
    public static void loginNico(Context context, String email, String password, final LoginCallback callback) {
        final PersistentCookieStore cookieStore = new PersistentCookieStore(context);
        final AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        // set cookie
        client.setCookieStore(cookieStore);
        // param login
        RequestParams params = new RequestParams();
        params.put("mail_tel", email);
        params.put("password", password);
        // login to get user session
        client.post(LOGIN_PATH, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onFail(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                // check header if sure login success
                boolean isFail = false;
                for (Header header : headers) {
                    if (header.getName().equalsIgnoreCase("x-niconico-authflag") && header.getValue().equalsIgnoreCase("0")) {
                        isFail = true;
                        break;
                    }
                }

                if (isFail) {
                    callback.onFail(statusCode, headers, responseString, null);
                } else {
                    String userSeasion = null;
                    for (Cookie findUserSS : cookieStore.getCookies()) {
                        if (findUserSS.getName().equalsIgnoreCase("user_session"))
                            userSeasion = findUserSS.getValue();
                    }
                    callback.onSuccess(client, userSeasion, cookieStore);
                }
            }
        });
    }

    /**
     * get nico dirrect link from url
     *
     * @param context
     * @param email
     * @param password
     * @param videoID
     * @param callback
     */
    public static void getNicoUrlAndCookie(Context context, String email, String password, final String videoID, final getVideoCallback callback) {
        loginNico(context, email, password, new LoginCallback() {
            @Override
            public void onSuccess(final AsyncHttpClient client, String userSeasion, final PersistentCookieStore cookieStore) {
                // get nico history
                client.setUserAgent(USER_AGENT);
                client.addHeader("user_session", userSeasion);
                final String finalUserSeasion = userSeasion;
                client.get(BASE_VIDEO_LINK + videoID, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        callback.onGetVideoIDFail(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        String nicoHistory = null;
                        for (Cookie findUserSS : cookieStore.getCookies()) {
                            if (findUserSS.getName().equalsIgnoreCase("nicohistory"))
                                nicoHistory = findUserSS.getValue();
                        }
                        // get direct link of video, please add nico history to play video
                        final String finalNicoHistory = nicoHistory;
                        client.get(GET_FLV_PATH + videoID, new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                callback.onFail(statusCode, headers, responseString, throwable);
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                try {
                                    int start = responseString.indexOf("&url=") + 5;
                                    int end = responseString.indexOf("&", start);
                                    String url = responseString.substring(start, end).trim();
                                    url = URLDecoder.decode(url, "utf-8");

                                    callback.onSuccess(finalUserSeasion, finalNicoHistory, url);
                                    client.cancelAllRequests(true);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
            }

            @Override
            public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onLoginFail(statusCode, headers, responseString, throwable);
            }
        });
    }

    /**
     * search video nico, require gson library
     *
     * @param context
     * @param page     start from 1
     * @param keyword  keyword to search
     * @param email
     * @param password
     * @param callback search callback {@link searchVideoCallback}
     */
    public static void searchVideo(final Context context, final int page, final String keyword, String email, String password, final searchVideoCallback callback) {
        // page must be greater than 0
        if (page > 0) {
            loginNico(context, email, password, new LoginCallback() {
                @Override
                public void onSuccess(final AsyncHttpClient client, String userSeasion, PersistentCookieStore cookieStore) {
                    client.addHeader("user_session", userSeasion);
                    client.get(SEARCH_PATH + page + "&searchType=keyword&words=" + keyword, new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            callback.onFail(statusCode, headers, responseString, throwable);
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                            Gson gson = new Gson();
                            List<RecycleItemObject> list = new ArrayList<>();
                            NicoSearchObject searchObject = gson.fromJson(responseString, NicoSearchObject.class);
                            List<NicoSearchDetailObject> searchDetails = searchObject.getList();

                            if (searchDetails != null) {
                                int type = context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? RecycleItemObject.LAYOUT_ITEM_PORTRAIN : RecycleItemObject.LAYOUT_ITEM_LANDSCAPE;
                                for (NicoSearchDetailObject detailObject : searchDetails) {

                                    // TODO: only supported "sm"
                                    if (detailObject.getId().startsWith("sm")) {
                                        list.add(new RecycleItemObject(type,
                                                detailObject.getId(),
                                                detailObject.getTitle(),
                                                null,
                                                detailObject.getFirst_retrieve(),
                                                String.valueOf(detailObject.getView_counter()),
                                                detailObject.getThumbnail_url(),
                                                detailObject.getLength(),
                                                detailObject.getDescription_short()));
                                    }
                                }

                                callback.onSuccess(searchObject.getPage(), list);
                            } else {
                                callback.onSuccess(-1, list);
                            }
                        }
                    });
                }

                @Override
                public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    callback.onLoginFail(statusCode, headers, responseString, throwable);
                }
            });
        } else {
            callback.onFail(404, null, "page must be greater than 0", null);
        }
    }

    /**
     * get top 100 video ranking, require jsoup library
     *
     * @param category video category: {@link #CATEGORY_ALL}, {@link #CATEGORY_ANIME_ART_GAMES}, {@link #CATEGORY_ENTERTAIMENT_MUSIC}, {@link #CATEGORY_LIFE_GENERAL_SPORT}, {@link #CATEGORY_OTHERS}, {@link #CATEGORY_POLITICS}, {@link #CATEGORY_SCIENCE_TECH}
     * @param lang     language: {@link #LANG_ENG}, {@link #LANG_JAP}
     * @param callback will call {@link getDailyRankingCallback}
     */
    public static void getDailyRanking(String category, String lang, final getDailyRankingCallback callback) {
        String url = RSS_PATH + category + "?rss=2.0&lang=" + lang;
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onFail(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                List<RecycleItemObject> list = new ArrayList<>();
                Document document = Jsoup.parse(responseString, "", Parser.xmlParser());
                Elements items = document.select("item");
                for (Element item : items) {
                    String videoId = item.select("link").text();
                    String videoTitle = item.select("title").text();
                    Document description = Jsoup.parse(item.select("description").text().trim(), "", Parser.xmlParser());

                    // TODO: only supported "sm"
                    if (videoId.substring(videoId.lastIndexOf("/") + 1).trim().startsWith("sm")) {
                        list.add(new RecycleItemObject(
                                RecycleItemObject.LAYOUT_ITEM_PORTRAIN,
                                videoId.substring(videoId.lastIndexOf("/") + 1).trim(),
                                videoTitle.substring(videoTitle.indexOf(":") + 1).trim(),
                                "",
                                description.select("strong.nico-info-date").text().trim(),
                                description.select("strong.nico-info-total-view").text().trim(),
                                description.select("img").attr("src").trim(),
                                description.select("strong.nico-info-length").text().trim(),
                                description.select("p.nico-description").text().trim()
                        ));
                    }
                }
                callback.onSuccess(list);
            }
        });
    }

    /**
     * get nico account info detail. <b>Require: jsoup library</b>
     *
     * @param context  context to create cookie store
     * @param email    email to login
     * @param password password
     * @param callback {@link getAccountDetailsCallback}
     */
    public static void getAccountDetails(Context context, final String email, String password, final getAccountDetailsCallback callback) {
        loginNico(context, email, password, new LoginCallback() {
            @Override
            public void onSuccess(AsyncHttpClient client, final String userSeasion, PersistentCookieStore cookieStore) {
                client.setCookieStore(cookieStore);
                client.get(MAIN_PATH, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        callback.onFail(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Document doc = Jsoup.parse(responseString);
                        String userName = doc.select("#siteHeaderUserNickNameContainer").text();
                        String userSeasionForGetAvatar = userSeasion.substring("user_session_".length(), userSeasion.lastIndexOf("_")).trim();
                        callback.onSuccess(email, userName.substring(0, userName.lastIndexOf(" ")).trim(), USER_ICON_PATH + userSeasionForGetAvatar.substring(0, 4) + "/" + userSeasionForGetAvatar + ".jpg");
                    }
                });
            }

            @Override
            public void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onLoginFail(statusCode, headers, responseString, throwable);
            }
        });
    }

    /**
     * get relative videos of video
     *
     * @param videoId  only support video id start with sm*****
     * @param page     dn't care about this parameter, it always 1
     * @param objects  an empty list
     * @param callback callback when get video relation success or fail
     */
    public static void getRelativeVideos(final String videoId, final int page, final List<RecycleItemObject> objects, final getRelativeVideosCallback callback) {
        final AsyncHttpClient client = new AsyncHttpClient();
        final RequestParams params = new RequestParams();
        params.put("video", videoId);
        params.put("page", page);
        client.get(RELATION_PATH, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onFailure(statusCode, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseRelativeVideos(client, responseString, objects);
            }

            private void parseRelativeVideos(AsyncHttpClient client, String responseString, List<RecycleItemObject> objects) {
                Document doc = Jsoup.parse(responseString);
                int total_page = Integer.parseInt(doc.select("page_count").first().text().trim());

                // if null break
                if (objects == null) return;
                for (Element e : doc.select("video")) {
                    // TODO: only supported "sm"
                    String url = e.select("url").text().trim();
                    if (url.substring(url.lastIndexOf("/") + 1).startsWith("sm")) {
                        objects.add(new RecycleItemObject(
                                RecycleItemObject.LAYOUT_ITEM_PORTRAIN,
                                url.substring(url.lastIndexOf("/") + 1),
                                e.select("title").text().trim(),
                                "author",
                                Utils.formatDateTimeString(Long.parseLong(e.select("time").text().trim()) * 1000, "MMM dd, yyyy hh : mm : ss"), // java unix = server response unix time * 1000
                                e.select("view").text().trim(),
                                e.select("thumbnail").text().trim(),
                                Utils.convertSecondsToHMmSs(Long.parseLong(e.select("length").text().trim()) * 1000),       // milisecond = second * 1000
                                "describe"
                        ));
                    }
                }

                // continue load all relation video
                if (page < total_page) {
                    // loop
                    getRelativeVideos(videoId, page + 1, objects, callback);
                } else {
                    // finish
                    callback.onSuccess();
                }
            }
        });
    }

    /**
     * cancel all thread
     *
     * @param client AsyncHttpClient object
     */
    public void cancelBackgroundThread(AsyncHttpClient client) {
        client.cancelAllRequests(true);
    }

    /**
     * callback for login
     */
    public interface LoginCallback {
        /**
         * login success
         *
         * @param client
         * @param userSeasion
         * @param cookieStore
         */
        void onSuccess(AsyncHttpClient client, String userSeasion, PersistentCookieStore cookieStore);

        /**
         * login fail
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable);
    }

    /**
     * nico video callback
     */
    public interface getVideoCallback {
        /**
         * fail when request
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable);

        /**
         * success
         *
         * @param userSession
         * @param nicoHistory
         * @param url
         * @throws UnsupportedEncodingException
         */
        void onSuccess(String userSession, String nicoHistory, String url) throws UnsupportedEncodingException;

        /**
         * fail when request to get video id
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onGetVideoIDFail(int statusCode, Header[] headers, String responseString, Throwable throwable);

        /**
         * login fail
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onLoginFail(int statusCode, Header[] headers, String responseString, Throwable throwable);
    }

    /**
     * video search callback
     */
    public interface searchVideoCallback {
        /**
         * if satus code is 404: nothing found, else: fail when request
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable);

        /**
         * cannot login
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onLoginFail(int statusCode, Header[] headers, String responseString, Throwable throwable);

        /**
         * * when search result > 0
         *
         * @param page current search page
         * @param list list recycle object
         */
        void onSuccess(int page, List<RecycleItemObject> list);
    }

    /**
     * callback of getdaily ranking
     */
    public interface getDailyRankingCallback {
        /**
         * when get rss fail
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable);

        /**
         * when get rss ranking success
         *
         * @param list
         */
        void onSuccess(List<RecycleItemObject> list);
    }

    public interface getAccountDetailsCallback {
        /**
         * when login fail
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onLoginFail(int statusCode, Header[] headers, String responseString, Throwable throwable);

        /**
         * when get html code to get account info fail
         *
         * @param statusCode
         * @param headers
         * @param responseString
         * @param throwable
         */
        void onFail(int statusCode, Header[] headers, String responseString, Throwable throwable);

        /**
         * when success to get account info
         *
         * @param email
         * @param userName
         * @param avatarUrl
         */
        void onSuccess(String email, String userName, String avatarUrl);
    }

    /**
     * callback for get relative videos
     */
    public interface getRelativeVideosCallback {
        void onFailure(int statusCode, String responseString, Throwable throwable);

        void onSuccess();
    }

    /**
     * Object return when search nico video
     *
     * @see NicoUtils.NicoSearchDetailObject
     */
    public class NicoSearchObject {
        private boolean has_ng_video_for_adsense_on_listing;
        private int page;
        private String status;
        private List<NicoSearchDetailObject> list;

        public NicoSearchObject(boolean has_ng_video_for_adsense_on_listing, int page, String status, List<NicoSearchDetailObject> list) {
            this.has_ng_video_for_adsense_on_listing = has_ng_video_for_adsense_on_listing;
            this.page = page;
            this.status = status;
            this.list = list;
        }

        public boolean isHas_ng_video_for_adsense_on_listing() {
            return has_ng_video_for_adsense_on_listing;
        }

        public void setHas_ng_video_for_adsense_on_listing(boolean has_ng_video_for_adsense_on_listing) {
            this.has_ng_video_for_adsense_on_listing = has_ng_video_for_adsense_on_listing;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<NicoSearchDetailObject> getList() {
            return list;
        }

        public void setList(List<NicoSearchDetailObject> list) {
            this.list = list;
        }
    }

    /**
     * Object nico video detail
     *
     * @see NicoUtils.NicoSearchObject
     */
    public class NicoSearchDetailObject {
        private String id;
        private String title;
        private String first_retrieve;
        private int view_counter;
        private int mylist_counter;
        private String thumbnail_url;
        private int num_res;
        private String last_res_body;
        private String length;
        private String title_short;
        private String description_short;
        private boolean is_middle_thumbnail;

        public NicoSearchDetailObject(String id, String title, String first_retrieve, int view_counter, int mylist_counter, String thumbnail_url, int num_res, String last_res_body, String length, String title_short, String description_short, boolean is_middle_thumbnail) {
            this.id = id;
            this.title = title;
            this.first_retrieve = first_retrieve;
            this.view_counter = view_counter;
            this.mylist_counter = mylist_counter;
            this.thumbnail_url = thumbnail_url;
            this.num_res = num_res;
            this.last_res_body = last_res_body;
            this.length = length;
            this.title_short = title_short;
            this.description_short = description_short;
            this.is_middle_thumbnail = is_middle_thumbnail;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFirst_retrieve() {
            return first_retrieve;
        }

        public void setFirst_retrieve(String first_retrieve) {
            this.first_retrieve = first_retrieve;
        }

        public int getView_counter() {
            return view_counter;
        }

        public void setView_counter(int view_counter) {
            this.view_counter = view_counter;
        }

        public int getMylist_counter() {
            return mylist_counter;
        }

        public void setMylist_counter(int mylist_counter) {
            this.mylist_counter = mylist_counter;
        }

        public String getThumbnail_url() {
            return thumbnail_url;
        }

        public void setThumbnail_url(String thumbnail_url) {
            this.thumbnail_url = thumbnail_url;
        }

        public int getNum_res() {
            return num_res;
        }

        public void setNum_res(int num_res) {
            this.num_res = num_res;
        }

        public String getLast_res_body() {
            return last_res_body;
        }

        public void setLast_res_body(String last_res_body) {
            this.last_res_body = last_res_body;
        }

        public String getLength() {
            return length;
        }

        public void setLength(String length) {
            this.length = length;
        }

        public String getTitle_short() {
            return title_short;
        }

        public void setTitle_short(String title_short) {
            this.title_short = title_short;
        }

        public String getDescription_short() {
            return description_short;
        }

        public void setDescription_short(String description_short) {
            this.description_short = description_short;
        }

        public boolean is_middle_thumbnail() {
            return is_middle_thumbnail;
        }

        public void setIs_middle_thumbnail(boolean is_middle_thumbnail) {
            this.is_middle_thumbnail = is_middle_thumbnail;
        }
    }
}
