package com.healtheworld.nico.helper.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.healtheworld.nico.R;
import com.healtheworld.nico.config.Setting;
import com.healtheworld.nico.custom.recycleview.RecycleAdapter;
import com.healtheworld.nico.custom.recycleview.RecycleItemObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by YuHung on 5/29/2015.
 */
public class DatabaseUtils {
    private static final int RESULT_NOT_INIT = 0;
    private static final int RESULT_SUCCESS = 1;
    private static final int RESULT_CONFLIC = 2;
    private static Context mContext;
    private static SQLiteDatabase sqlite;

    public static void initial(Context context) {
        mContext = context;
        SqliteHelper helper = new SqliteHelper(context, Setting.getDatabaseName(), null, Setting.getDatabaseVerson());
        // make sure close any cursor
        helper.close();
        sqlite = helper.getWritableDatabase();
    }

    /**
     * get all item when click navigation -> refresh main data
     *
     * @param name playlist container name
     * @return list data {@link RecycleItemObject}
     */
    public static List<RecycleItemObject> getPlaylistByName(String name) {
        List<RecycleItemObject> list = new ArrayList<>();
        if (sqlite != null) {
            // get layout type
            int type = mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? RecycleItemObject.LAYOUT_ITEM_LANDSCAPE : RecycleItemObject.LAYOUT_ITEM_PORTRAIN;

            // get fav id
            Cursor query = sqlite.query(Setting.getTableFavContainer(), null, "name='" + name.trim() + "'", null, null, null, null);
            String favId = null;
            if (query != null && query.moveToFirst())
                favId = query.getString(query.getColumnIndex("_id"));

            // fill data to list
            if (favId != null) {
                Cursor cursor = sqlite.query(Setting.getTableFavItem(), null, "favContainer=" + favId, null, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        RecycleItemObject object = new RecycleItemObject(
                                type,
                                cursor.getString(cursor.getColumnIndex("videoId")),
                                cursor.getString(cursor.getColumnIndex("title")),
                                cursor.getString(cursor.getColumnIndex("author")),
                                cursor.getString(cursor.getColumnIndex("uploadDate")),
                                cursor.getString(cursor.getColumnIndex("views")),
                                cursor.getString(cursor.getColumnIndex("thumb")),
                                cursor.getString(cursor.getColumnIndex("duration")),
                                null
                        );

                        // this is hidden option
                        object.setIsFav(true);
                        list.add(object);

                        // next cursor
                        cursor.moveToNext();
                    }
                }
            }
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return list;
    }

    /**
     * clear & add new account to local database
     *
     * @param email
     * @param password
     * @param name     account anme
     * @param avatar   avatar image url
     * @return {@link #RESULT_SUCCESS}, {@link #RESULT_NOT_INIT}
     */
    public static final int cleanAndAddAccount(String email, String password, String name, String avatar) {
        int result = RESULT_NOT_INIT;
        if (sqlite != null) {
            // delete all fist
            deleteAllAccountSaved();

            ContentValues cv = new ContentValues();
            try {
                cv.put("email", encrypt(email));
                cv.put("password", encrypt(password));
                cv.put("name", encrypt(name));
                cv.put("avatar", encrypt(avatar));
                sqlite.insert(Setting.getTableAccountName(), null, cv);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            result = RESULT_SUCCESS;
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }

        return result;
    }

    /**
     * @return get account saved in database
     */
    public static final AccountObject getAccountSaved() {
        AccountObject obj = null;
        if (sqlite != null) {
            Cursor cursor = sqlite.query(Setting.getTableAccountName(), null, null, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                // add data to object && decrypt data
                obj = new AccountObject();
                obj.setEmail(decrypt(cursor.getString(cursor.getColumnIndex("email"))));
                obj.setPassword(decrypt(cursor.getString(cursor.getColumnIndex("password"))));
                obj.setName(decrypt(cursor.getString(cursor.getColumnIndex("name"))));
                obj.setAvatar(decrypt(cursor.getString(cursor.getColumnIndex("avatar"))));

                cursor.close();
            }
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return obj;
    }

    /**
     * delete account which saved before
     *
     * @return {@link #RESULT_SUCCESS}, {@link #RESULT_NOT_INIT}
     */
    public static final int deleteAllAccountSaved() {
        int result = RESULT_NOT_INIT;
        if (sqlite != null) {
            sqlite.delete(Setting.getTableAccountName(), null, null);
            result = RESULT_SUCCESS;
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }

        return result;
    }

    /**
     * add fav container object to database
     *
     * @param name name of container
     * @return id inserted if success, {@link #RESULT_NOT_INIT}, {@link #RESULT_CONFLIC}
     */
    public static int addFavContainer(String name) {
        int result = RESULT_NOT_INIT;
        if (sqlite != null) {
            // check fav container before add
            Cursor cursor = sqlite.query(Setting.getTableFavContainer(), null, "name='" + name.trim() + "'", null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                result = RESULT_CONFLIC;
                Log.e(Setting.getLogTag(), "fav container name must be unique.");
            } else {
                ContentValues cv = new ContentValues();
                cv.put("name", name.trim());
                result = (int) sqlite.insert(Setting.getTableFavContainer(), null, cv);
            }

            if (cursor != null) cursor.close();
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return result;
    }

    /**
     * delete fav container by id
     *
     * @param id fav container's id
     * @return {@link #RESULT_SUCCESS}, {@link #RESULT_NOT_INIT}
     */
    public static int deleteFavContainer(int id) {
        int result = RESULT_NOT_INIT;
        if (sqlite != null) {
            sqlite.delete(Setting.getTableFavContainer(), "id=" + id, null);

            // delete item which stay in container
            Cursor cursor = sqlite.query(Setting.getTableFavItem(), null, "favContainer=" + id, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    // delete all fav item found
                    deleteFavItem(cursor.getInt(cursor.getColumnIndex("id")));
                    cursor.moveToNext();
                }
                cursor.close();
            }
            result = RESULT_SUCCESS;
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }

        return result;
    }

    /**
     * @return get all fav container cursor
     * @see com.healtheworld.nicovideoplayer.custom.recycleview.Holder#generatePopupMenu(View, int, Context, AlertDialog.Builder, RecycleAdapter, RecycleAdapter.ItemSubMenu)
     */
    public static Cursor getFavContainer() {
        Cursor cursor = null;
        if (sqlite != null) {
            cursor = sqlite.query(Setting.getTableFavContainer(), null, null, null, null, null, null);

            // add new fav container item ...
            MatrixCursor extras = new MatrixCursor(cursor.getColumnNames());
            extras.addRow(new String[]{"-1", mContext.getString(R.string.add_new_fav_container_item)});
            cursor = new MergeCursor(new Cursor[]{extras, cursor});
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return cursor;
    }

    /**
     * @param favContainerID ID of fav container
     * @return number item inside container which have param "favContainerID"
     */
    public static int countItemInFavContainer(int favContainerID) {
        int count = -1;
        if (sqlite != null) {
            Cursor cursor = sqlite.query(Setting.getTableFavItem(), null, "favContainer=?", new String[]{String.valueOf(favContainerID)}, null, null, null);
            count = cursor.getCount();
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return count;
    }

    /**
     * add fav item to database
     *
     * @param videoId      video id
     * @param title        video name
     * @param author       uploader
     * @param uploadDate   upload date
     * @param views        view count
     * @param thumb        link video thumbnail
     * @param duration     video duration (hh:mm:ss)
     * @param favContainer id of container which contain fav item
     * @return {@link #RESULT_SUCCESS}, {@link #RESULT_NOT_INIT}
     */
    public static int addFavItem(String videoId, String title, String author, String uploadDate, String views, String thumb, String duration, int favContainer) {
        int result = RESULT_NOT_INIT;
        if (sqlite != null) {
            ContentValues cv = new ContentValues();
            cv.put("videoId", videoId);
            cv.put("title", title);
            cv.put("author", author);
            cv.put("uploadDate", uploadDate);
            cv.put("views", views);
            cv.put("thumb", thumb);
            cv.put("duration", duration);
            cv.put("favContainer", favContainer);

            sqlite.insert(Setting.getTableFavItem(), null, cv);
            result = RESULT_SUCCESS;
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return result;
    }

    /**
     * delete fav item by id
     *
     * @param id fav item's id
     * @return {@link #RESULT_SUCCESS}, {@link #RESULT_NOT_INIT}
     */
    public static int deleteFavItem(int id) {
        int result = RESULT_NOT_INIT;
        if (sqlite != null) {
            sqlite.delete(Setting.getTableFavItem(), "id=" + id, null);
            result = RESULT_SUCCESS;
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return result;
    }

    /**
     * check video is inserted in same fav container
     *
     * @param videoId        video id
     * @param favContainerId fav container id
     * @return true: if exist
     * @see RecycleAdapter#addFavItem(int, int)
     */
    public static boolean isExistItemInFavContainer(String videoId, int favContainerId) {
        Cursor cursor = null;
        if (sqlite != null) {
            cursor = sqlite.query(Setting.getTableFavItem(), null, "videoId=? and favContainer=?", new String[]{videoId, String.valueOf(favContainerId)}, null, null, null);
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        boolean result = cursor == null ? false : cursor.getCount() > 0;
        cursor.close();
        return result;
    }

    public static List<String> getAllFavItems() {
        List<String> list = null;
        if (sqlite != null) {
            Cursor cursor = sqlite.query(Setting.getTableFavItem(), null, null, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                list = new ArrayList<>();
                while (!cursor.isAfterLast()) {
                    list.add(cursor.getString(cursor.getColumnIndex("videoId")));
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } else {
            Log.e(Setting.getLogTag(), "you have to call DatabaseUtils.initial()");
        }
        return list;
    }

    /**
     * encrypt data before save to db
     *
     * @param str string to encrypt
     * @return
     * @throws UnsupportedEncodingException
     */
    public static final String encrypt(String str) throws UnsupportedEncodingException {
        return str != null ? new String(Base64.encode((str + Setting.getDefaultKey()).getBytes("utf-8"), Base64.NO_PADDING)) : null;
    }

    /**
     * decrypt data when use
     *
     * @param encrypted
     * @return
     */
    public static final String decrypt(String encrypted) {
        String result = null;
        if (encrypted != null) {
            String temp = new String(Base64.decode(encrypted, Base64.NO_PADDING));
            if (temp.length() > Setting.getDefaultKey().length()) {
                result = temp.substring(0, temp.length() - Setting.getDefaultKey().length());
            }
        }
        return result;
    }

    private static class SqliteHelper extends SQLiteOpenHelper {
        public SqliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // create account table
            db.execSQL("CREATE TABLE " + Setting.getTableAccountName() + " (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, email TEXT NOT NULL, password TEXT NOT NULL, name TEXT NOT NULL,avatar TEXT NOT NULL)");

            // create fav container
            db.execSQL("CREATE TABLE " + Setting.getTableFavContainer() + " (_id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT  UNIQUE NOT NULL)");

            // create fav item
            db.execSQL("CREATE TABLE " + Setting.getTableFavItem() + " (_id INTEGER  PRIMARY KEY NOT NULL, videoId TEXT  NOT NULL, title TEXT  NOT NULL, author TEXT  NOT NULL, uploadDate TEXT  NOT NULL, views TEXT  NOT NULL, thumb TEXT  NOT NULL, duration TEXT  NOT NULL, favContainer INTEGER NOT NULL)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // delete account table
            db.execSQL("DROP TABLE IF EXISTS " + Setting.getTableAccountName());

            // delete fav container table
            db.execSQL("DROP TABLE IF EXISTS " + Setting.getTableFavContainer());

            // delete fav item table
            db.execSQL("DROP TABLE IF EXISTS " + Setting.getTableFavItem());
            onCreate(db);
        }
    }
}
