package com.healtheworld.nico.helper.database;

/**
 * Save account info when user fragment_login, all info will be encrypted
 * Created by YuHung on 4/27/2015.
 */
public class AccountObject {
    private int id;
    private String email;
    private String password;
    private String name;
    private String avatar;

    /**
     * get account id
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * set account id
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * get email, please decrypt to use
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * set email encrypted
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * get password, please decrypt to use
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * set password encrypted
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * get name, please decrypt to use
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set name encrypted
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get thumb, please decrypt to use
     *
     * @return
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * set thumb encrypted
     *
     * @param avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
